import os

root = os.getcwd()
datadir = os.path.join(root, r'testdata/Olis')
list_of_filenames = [os.path.join(datadir, fname) for fname in os.listdir(datadir)]
filename = list_of_filenames[0]

import Instruments

olis = Instruments.Olis()

imported_spectrum = olis.load(filename)
# imported_spectrum.show()

imported_series = Instruments.PhotoSeries()
for filename in list_of_filenames:
	new_spectrum = olis.load(filename)
	imported_series.append(new_spectrum)

imported_series.show()
average = imported_series.average()
average.show(append=False)

filtered = imported_series.savgol_filter(window_length=11, order=3)

concentrations = [0.2, 0.4, 0.6, 0.8, 1.0, 1.2, 1.4]
p, r2 = filtered.calibrate(concentrations, 470, plot=True)
print(p, r2)
#p is a tuple containing the coefficients describing the line-of-best-fit for the Beer-Lambert plot
#r2 is the r-squared of the fitted line
