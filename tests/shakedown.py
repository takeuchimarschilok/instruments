'''
Script for automated testing of spectrum import functionality.

Tests loading functionality for datasets acquired on the following
instruments:

    Olis UV-Vis
    Genesys UV-Vis
    Horiba Confocal Raman
    MACCOR
    Rigaku Powder XRD
'''

import os

import Instruments

root = os.getcwd()

datadir = os.path.join(root, 'testdata/Olis')
files = [os.path.join(datadir, fname) for fname in os.listdir(datadir)]
olisdata = Instruments.PhotoSeries()
for fname in files:
    olisdata.append(Instruments.Olis.load(fname))
print('Load Olis data: PASS')

datadir = os.path.join(root, 'testdata/Horiba')
fname = os.path.join(datadir, 'horiba_testdata.txt')
horibadata = Instruments.Horiba.load_series(fname)
print('Load confocal Horiba data: PASS')

datadir = os.path.join(root, 'testdata/Rigaku')
files = [os.path.join(datadir, fname) for fname in os.listdir(datadir)]
rigakudata = Instruments.XRDSeries()
for fname in files:
    rigakudata.append(Instruments.Rigaku.load(fname))
print('Load Rigaku data: PASS')

datadir = os.path.join(root, 'testdata/MACCOR')
files = [os.path.join(datadir, fname) for fname in os.listdir(datadir)]
maccordata = Instruments.CVSeries()
for fname in files:
    maccordata.append(Instruments.MACCOR.load(fname))
print('Load MACCOR data: PASS')

datadir = os.path.join(root, 'testdata/BioLogic')
fname = os.path.join(datadir, 'biologictest1.mpt')
biologicdata = Instruments.BioLogic.load(fname)
print('Load BioLogic data: PASS')
