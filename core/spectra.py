# -*- coding: utf-8 -*-
"""
Specialized derivatives of the core Spectrum class, designed to handle specific
spectroscopic or other data types.

Created on Wed May 27 11:50:57 2020

@author: tyler
"""

import matplotlib.pyplot as plt
from .core import Spectrum, find_nearest_member, _migrate_params_, apply_polynomial, RMSE
import numpy as np
from scipy.optimize import minimize_scalar

class PhotoSpectrum(Spectrum):
    '''
    Specialized Spectrum class for spectrophotometric spectra.
    '''

    def __init__(self, xy_tuple, *args, **kwds):

        #Data validity checks
        if len(xy_tuple) != 2:
            raise ValueError('xy_tuple must have 2 and only 2 elements')
        x, y = xy_tuple
        if len(x) != len(y):
            raise ValueError('Both xy_tuple elements must have the same length')

        Spectrum.__init__(self, xy_tuple, *args, **kwds)
        return

    @staticmethod
    def estimate_concentration(absorbance, coeff):
        '''
        Calculate concentration from measured absorbance
        '''
        m, b = coeff
        return (absorbance - b) / m

class RamanSpectrum(PhotoSpectrum):

    def __init__(self, xy_tuple, *args, **kwds):
        PhotoSpectrum.__init__(self, xy_tuple, *args, **kwds)
        return

    def find_peaks(self):

        '''Modified wrapper of peak finder to account for Lorentzian
           distribution of Raman peaks.'''

        return self.find_peaks(self, wavelet='lorentzian')

    def spike_detect(self, aggression=3):
        threshold = 1/aggression
        norm = self.normalize()
        derivative = np.abs(np.diff(norm.y))
        spikes = np.where(derivative >  threshold)
        return spikes

class XRDSpectrum(Spectrum):

    def __init__(self, xy_tuple):
        Spectrum.__init__(self, xy_tuple)
        return

class CVSpectrum(Spectrum):

    def __init__(self, cycle_dict):
        Spectrum.__init__(self, (None, None))
        self.charge = cycle_dict['charge']
        self.discharge = cycle_dict['discharge']
        return

    def show(self, append=True, *args, **kwds):
        if not append:
            plt.figure()

        if 'alpha' in kwds: alpha = kwds['alpha']
        else: alpha = 1

        plt.plot(self.charge['capacity'], self.charge['voltage'], color='tab:blue', alpha=alpha)
        plt.plot(self.discharge['capacity'], self.discharge['voltage'], color='tab:orange', alpha=alpha)
        plt.ylabel('Voltage (V)')
        plt.xlabel('Capacity (mA h)')
        return

    def calc_specific_energy(self, active_mass):
        charge_specific = self.charge['energy']/active_mass
        discharge_specific = self.discharge['energy']/active_mass
        self.charge['specific_energy'] = charge_specific
        self.discharge['specific_energy'] = discharge_specific
        try: specific_energy = {'charge':charge_specific[-1], 'discharge':discharge_specific[-1]}
        except IndexError:
            specific_energy = {'charge':0, 'discharge':0}
        return specific_energy

    def calc_specific_capacity(self, active_mass):
        charge_specific = self.charge['capacity']/active_mass
        discharge_specific = self.discharge['capacity']/active_mass
        self.charge['specific_capacity'] = charge_specific
        self.discharge['specific_capacity'] = discharge_specific
        return {'charge':charge_specific[-1], 'discharge':discharge_specific[-1]}

    def calc_areal_capacity(self, area):
        charge_specific = self.charge['capacity']/area
        discharge_specific = self.discharge['capacity']/area
        self.charge['areal_capacity'] = charge_specific
        self.discharge['areal_capacity'] = discharge_specific
        return {'charge':charge_specific[-1], 'discharge':discharge_specific[-1]}

    def calc_volumetric_capacity(self, volume):
        charge_specific = self.charge['capacity']/volume
        discharge_specific = self.discharge['capacity']/volume
        self.charge['volumetric_capacity'] = charge_specific
        self.discharge['volumetric_capacity'] = discharge_specific
        return {'charge':charge_specific[-1], 'discharge':discharge_specific[-1]}

class TimeSeries(Spectrum):
    '''
    Adaptation of Spectrum class to handle generalized time-series data such as
    chromatography or electrochemical measurements.
    '''
    def __init__(self, xy_tuple):
        Spectrum.__init__(self, xy_tuple)
        return

class Chromatogram(TimeSeries):
    
    def __init__(self, xy_tuple, t0=0):
        TimeSeries.__init__(self, xy_tuple)
        self.x_abs = np.array(self.x)
        self.t0 = t0
        self.mode = 'time'
        return
        
    def set_t0(self, t0):
        self.t0 = t0
        self.x -= t0
        return
    
    def get_retention_volume(self, flow_rate):
        '''Flow rate should be given in units of mL/min'''
        assert self.mode == 'time'
        self.x = self.x / 60 * flow_rate
        return

class XASSpectrum(Spectrum):
    '''
    '''

    def __init__(self, xy_tuple):
        Spectrum.__init__(self, xy_tuple)
        self.edge = self.find_edge()
        self.pre_edge_bounds = (self.edge[0]-200, self.edge[0]-30)
        self.post_edge_bounds = (self.edge[0]+50, self.edge[0]+3000)
        try:
            self.fit_pre_edge()
        except:
            # print('Warning: Could not fit pre-edge line')
            self.pre_edge = None
        try:
            self.fit_post_edge()
        except:
            # print('Warning: Could not fit post-edge line')
            self.post_edge = None 
        return
    
    def fit_pre_edge(self):
        a = self.x >= self.pre_edge_bounds[0]
        b = self.x <= self.pre_edge_bounds[1]
        irange = [a[i] and b[i] for i in range(len(a))]
        x = self.x[irange]
        y = self.y[irange]
        coeff = np.polyfit(x, y, 1)
        pre_edge = apply_polynomial(self.x, coeff)
        self.pre_edge = Spectrum((self.x, pre_edge))
        return
        
    def fit_post_edge(self):
        a = self.x >= self.post_edge_bounds[0]
        b = self.x <= self.post_edge_bounds[1]
        irange = [a[i] and b[i] for i in range(len(a))]
        x = self.x[irange]
        y = self.y[irange]
        coeff = np.polyfit(x, y, 2)
        post_edge = apply_polynomial(self.x, coeff)
        self.post_edge = Spectrum((self.x, post_edge))
        return

    def find_edge(self):
        der = self.derivative()
        d_max = np.where(der.y==der.y.max())[0][0]
        edge_energy = self.x[d_max]
        edge_intensity = self.y[d_max]
        return (edge_energy, edge_intensity)

    def split_xafs(self, cutoff=30, keep_params=True):
        assert self.edge_energy is not None, "Edge energy must be specified before XAFS can be isolated."
        lower_bound = self.edge[0] + cutoff
        split_point = find_nearest_member(self.x, lower_bound)
        new_x = self.x[split_point:]
        new_y = self.y[split_point:]
        xafs = XAFSSpectrum((new_x, new_y))
        if keep_params:
            _migrate_params_(self, xafs)
        xafs.edge = self.edge
        return xafs

    def split_xanes(self, cutoff=30, keep_params=True):
        assert self.edge_energy is not None, "Edge energy must be specified before XANES can be isolated."
        upper_bound = self.edge[0] + cutoff
        split_point = find_nearest_member(self.x, upper_bound)
        new_x = self.x[:split_point]
        new_y = self.y[:split_point]
        xanes = XANESSpectrum((new_x, new_y))
        if keep_params:
            _migrate_params_(self, xanes)
        xanes.edge = self.edge
        return xanes
    
    def normalize(self):
        normalized = self.copy()
        i_edge = find_nearest_member(self.x, self.edge[0])
        normalized -= self.pre_edge
        normalized.post_edge -= self.pre_edge
        normalized.y = normalized.y / normalized.post_edge.y
        return normalized
    
    def calibrate(self, reference_energy):
        calibrated = self.copy()
        shift = calibrated.edge[0] - reference_energy
        calibrated.x -= shift
        calibrated.edge[0] = reference_energy
        return calibrated
    
    def align(self, reference, erange=(-30,60)):
        
        erange = (self.edge[0]+erange[0], self.edge[0]+erange[1])
        d_self = self.slice(erange[0], erange[1]).derivative().savgol_filter(7,3).normalize()
        d_ref = reference.interpolate(d_self.x).derivative().savgol_filter(7,3).normalize()
        
        def error(shift):
            return RMSE(d_self+shift, d_ref)
        
        result = minimize_scalar(error, method='bounded', bounds=(-10,0))
        
        return result
        
    # def align:
    #     return

class XANESSpectrum(XASSpectrum):

    def __init__(self, xy_tuple):
        XASSpectrum.__init__(self, xy_tuple)
        return


class XAFSSpectrum(XASSpectrum):

    def __init__(self, xy_tuple):
        XASSpectrum.__init__(self, xy_tuple)
        return

    def k_weight(self, weight=1, keep_params=True):
        # assert self.parameters['x_axis'] == 'k', 'Spectrum must be in k-space to apply k weighting.'
        weighted = self.copy(keep_params=keep_params)
        for i, k in enumerate(self.x):
            weighted.y[i] = self.y[i] * (k**weight)
        # weighted.edge_energy = self.edge_energy
        return weighted

    def E_to_k(self, keep_params=True):
        '''
        Converts spectral axis from units of eV to cm-1
        '''
        assert self.edge_energy is not None, "Edge energy must be specified before XAFS can be converted to k-space."
        m = 9.1093837015E-34
        h_bar = 6.58211956E-16
        k_axis = []
        for e in self.x:
            if e < self.edge_energy:
                k = (1 / h_bar) * -np.sqrt(2*m*np.abs(e-self.edge_energy))
            else:
                k = (1 / h_bar) * np.sqrt(2*m*(e-self.edge_energy))
            k_axis.append(k)
        converted = XAFSSpectrum((k_axis, self.y))
        if keep_params:
            _migrate_params_(self, converted)
        converted.parameters['x_axis'] = 'k'
        converted.edge_energy = self.edge_energy
        return converted

    def k_to_E(self, keep_params=True):
        '''
        Converts spectral axis from units of cm-1 to eV
        '''
        assert self.edge_energy is not None, "Edge energy must be specified before XAFS can be converted to k-space."
        m = 9.1093837015E-34
        h_bar = 6.58211956E-16
        e_axis = []
        for k in self.x:
            e = (((k*h_bar)**2) / (2*m)) - self.edge_energy
            e_axis.append(e)
        converted = converted = XAFSSpectrum((e_axis, self.y))
        if keep_params:
            _migrate_params_(self, converted)
        converted.edge_energy = self.edge_energy
        converted.parameters['x_axis'] = 'energy'

    def normalize(self, mode='area', keep_params=True):

        def areanorm(self):
            yabs = np.abs(self.y)
            normalization_factor = np.sum(yabs)
            normalized = XAFSSpectrum((self.x, self.y/normalization_factor))
            return normalized

        def bpnorm(self):
            minmax = (np.abs(self.y.min()), np.abs(self.y.max()))
            normalization_factor = max(minmax)
            normalized = XAFSSpectrum((self.x, self.y/normalization_factor))
            return normalized

        modes = {'area':areanorm, 'basepeak':bpnorm}
        normfunction = modes[mode]
        normalized = normfunction(self)

        if keep_params:
            for param, value in self.parameters.items():
                normalized.parameters[param] = value

        return normalized

    # def k_to_R(self, keep_params=True):
    #     return

    # def fourier(self, bounds):
    #     return

class MassSpectrum(Spectrum):
    
    def __init__(self, xy_tuple):
        Spectrum.__init__(self, xy_tuple)
        return

