# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 16:16:13 2020

Base classes and helper functions for Takeuchi group instrumental analysis
codebase. Contains objects which are broadly applicable across multiple
instruments and data types.

@author: Steven King

Be gay, do crime
"""

import numpy as np
from matplotlib.axes import Subplot
import matplotlib.pyplot as plt
from scipy import signal
from scipy.optimize import minimize, LinearConstraint
import h5py as hf
import warnings
# from .ref import *
# from .gui import tGUI

# spectrum_classes = set(Spectrum, PhotoSpectrum)
# series_classes = set(Series, PhotoSeries)

class Spectrum:
    '''
    Base Spectrum object. For handling and analyzing generalized spectroscopic
    data. Spectrum class forms the basis for several child classes, including
    PhotoSpectrum for photometric data and CVSpectrum for cyclic voltammetry
    analysis.

    Supports addition, subtraction, multiplication, and division, with both other
    Spectrum (and Spectrum derivative) objects and constant values.

    Supports iteration and indexing along intensity axis.

    Contains additional helper methods for spectral processing and analysis,
    including spectral smoothing, normalization, and peak detection.
    '''
    def __init__(self, xy_tuple, *args, **kwds):
        '''
        Initialize Spectrum object. Arbitrary metaparameters may be supplied as keywords.

        Input:
            xy_tuple: (x, y)
                x and y are arraylike objects containing the values of the
                spectral and intensity axes of the new spectrum

            arbitrary keywords may be used to populate the attribute Spectrum.parameters.
            This dictionary is intended for the storage of general spectral metadata.
        '''

        x, y = xy_tuple

        #Deconstruct input tuple, store elements as spectral and intensity axes

        #Sort so all spectral axis values increase in order along array
        y = proxy_sort(x, y)
        x = np.sort(x)

        self.x = np.array(x)
        self.y = np.array(y)
        #Dump any supplied metadata to parameters attribute
        self.parameters = {k:v for k, v in kwds.items()}
        return

    def __str__(self):
        '''
        Prints values of x and y variables and all metaparameters to console.
        '''
        print(self.x)
        print(self.y)
        if len(self.parameters) > 0: print('Parameters:')
        for k, v in self.parameters.items():
            print(k, ':', v)
        return ''

    def __add__(self, other):
        '''
        Add spectral intensity axes, or add a constant value

        Input:
            other: variable types
                if Spectrum or derivative type, add other.y to self.y element-wise
                if int or float, add other to all elements of self.y

        Output:
            new_spec: Spectrum or derivative type
                Resulting sum spectrum, of same type as self
        '''
        #Create copy of self to populate values
        new_spec = self.copy()
        try:
            #First, attempt to add spectra element-wise
            new_spec.y = self.y + other.y
        except AttributeError:
            #If element-wise fails, add value to all intensities
            new_spec.y = self.y + other
        return new_spec

    def __radd__(self, other):
        if other == 0:
            return self
        else:
            return self.__add__(other)

    def __sub__(self, other):
        '''
        Subtract spectral intensity axes, or subtract a constant value

        Input:
            other: variable types
                if Spectrum or derivative type, other.y from self.y element-wise
                if int or float, subtract other from all elements of self.y

        Output:
            new_spec: Spectrum or derivative type
                Resulting difference spectrum, of same type as self
        '''
        #Create copy of self to populate values
        new_spec = self.copy()
        try:
            #First, attempt to subtract spectra element-wise
            new_spec.y = self.y - other.y
        except AttributeError:
            #If element-wise fails, subtract value from all intensities
            new_spec.y = self.y - other
        return new_spec

    def __truediv__(self, other):
        '''
        Divide spectral intensity axes, or divide by a constant value

        Input:
            other: variable types
                if Spectrum or derivative type, divide self.y by other.y element-wise
                if int or float, divide all elements of self.y by other

        Output:
            new_spec: Spectrum or derivative type
                Resulting ratio spectrum, of same type as self
        '''
        #Create copy of self to populate values
        new_spec = self.copy()
        try:
            #First, attempt to divide spectra element-wise
            new_spec.y = self.y / other.y
        except AttributeError:
            #If element-wise fails, divide all intensities by value
            new_spec.y = self.y / other
        return new_spec

    def __mul__(self, other):
        '''
        Multiply spectral intensity axes, or multiply by a constant value

        Input:
            other: variable types
                if Spectrum or derivative type, multiply self.y by other.y element-wise
                if int or float, multiply all elements of self.y by other

        Output:
            new_spec: Spectrum or derivative type
                Resulting convolved spectrum, of same type as self
        '''
        #Create copy of self to populate values
        new_spec = self.copy()
        try:
            #First, attempt to multiply spectra element-wise
            new_spec.y = self.y * other.y
        except AttributeError:
            #If element-wise fails, multiply all intensities by value
            new_spec.y = self.y * other
        return new_spec

    def __len__(self):
        '''
        Returns integer length of spectral intensity axis.
        '''
        return len(self.y)

    def __iter__(self):
        '''
        Initialize iteration of spectral intensty axis.
        '''
        self.iter_count = 0
        return self

    def __next__(self):
        '''
        Advance iteration of spectral intensty axis.
        '''
        if self.iter_count < len(self.y):
            result = self.y[self.iter_count]
            self.iter_count += 1
            return result
        else:
            raise StopIteration

    def __getitem__(self, i):
        '''
        Implement indexing of Spectrum object, returns ith element of self.y
        '''
        return self.y[i]

    def __neg__(self):
        negated = self.copy(keep_params=True)
        negated.y = -negated.y
        return negated

    def _decompose_(self):
        '''
        Decompose Spectrum into tuple containing spectral and intensity axes
        and any metadata parameters
        '''
        return (self.x, self.y, self.parameters)

    def min(self):
        '''
        Return minimum value of intensty axis.
        '''
        return min(self.y)

    def max(self):
        '''
        Return maximum value of intensity axis.
        '''
        return max(self.y)
    
    def abs(self):
        new_spec = self.copy(keep_params=True)
        new_spec.y = np.abs(self.y)
        return new_spec
    
    def sum(self):
        return self.y.sum()
    
    def log(self, base=10, keep_params=True):
        '''
        Applies logarithmic scaling to the spectral (y) axis, returns a new
        spectrum.

        Parameters
        ----------
        base : float, optional
            Logarithm base applied to the spectral axis. The default is 10.
        keep_params : bool, optional
            Determines spectrum metadata inheritance. The default is True.

        Returns
        -------
        new_spec : Spectrum
            Logarithmically scaled spectrum.
        '''
        
        new_spec = self.copy(keep_params=keep_params)
        new_spec.y = np.emath.logn(base, self.y)
        return new_spec
    
    def exp(self, exp, keep_params=True):
        '''
        Applies logarithmic scaling to the spectral (y) axis, returns a new
        spectrum.

        Parameters
        ----------
        exp : float,
            Exponent for intensity scaling.
        keep_params : bool, optional
            Determines spectrum metadata inheritance. The default is True.

        Returns
        -------
        new_spec : Spectrum
            Exponentially scaled spectrum.
        '''
        new_spec = self.copy(keep_params=keep_params)
        new_spec.y = self.y ** exp
        return new_spec

    def copy(self, keep_params=True):
        '''
        Return an identical duplicate copy of self. Optionally passes on
        metadata parameters to the new copy.
        '''

        #Generate new Spectrum object of same type as self, populate with
        #self.x and self.y
        spec_type = type(self)
        new_spec = spec_type((self.x, self.y))

        #If keep_params, copy all keywords and values to new Spectrum
        if keep_params:
            for k, v in self.parameters.items():
                new_spec.parameters[k] = v
        return new_spec

    def _copy_empty_(self, keep_params=False):
        new_spec = self.copy(keep_params=keep_params)
        new_spec.y[:] = 0
        return

    def slice(self, min, max, keep_params=True):
        '''
        Slice a spectrum to extract only a subset of the original. Returns
        region of original spectrum bounded by provided values for min and max.

        Input:
            min: int or float
                Lower bound of extracted slice, in units/values of spectral axis
            max: int or float
                Upper bound of extracted slice, in units/values of spectral axis

        Output:
            new_spec: same as self
                Extracted spectral band, of same type as self
        '''
        #Find indices corresponding to user-input upper and lower bounds
        i_low = find_nearest_member(self.x, min)
        i_high = find_nearest_member(self.x, max) + 1

        #Create new copy of self to populate with values, dumping existing metadata
        new_spec = self.copy(keep_params=keep_params)
        #Extract array slices bounded by matched indices
        new_spec.x = self.x[i_low:i_high]
        new_spec.y = self.y[i_low:i_high]
        return new_spec

    def set(self, bounds, value=0):
        '''
        Sets spectral intensity to input value on the specified range.
        '''

        low, high = bounds
        i_low = find_nearest_member(self.x, low)
        i_high = find_nearest_member(self.x, high)
        self.y[i_low:i_high] = value
        return

    def show(self, mode='plot', append=True, *args, **kwds):
        '''
        Plots spectrum using Matplotlib.

        Plot can be drawn in either 'plot' or 'scatter' mode by using the
        corresponding mode keyword.

        Input:
            mode: str, default='plot'
                Selected draw mode for new plot. Options are:
                plot - line plot
                scatter - scatter plot
            append: bool, default=True
                If True, adds plot to the active matplotlib figure
                If False, initializes a new matplotlib figure
            alpha: float, default=1
                Opacity of new plot, from 0 to 1 (0% - 100% opacity)
        '''
        if 'alpha' in kwds: alpha = kwds['alpha']
        else: alpha = 1
        if 'color' in kwds: color = kwds['color']
        else: color = None


        if append is False:
            fig = plt.figure()
            ax = fig.add_subplot(111)
        elif type(append) is Subplot:
            ax = append
        else:
            fig = plt.gcf()
            try:
                ax = fig.axes[0]
            except IndexError:
                ax = fig.add_subplot(111)

        if mode == 'plot':
            ax.plot(self.x, self.y, *args, **kwds)

        elif mode == 'scatter':
            ax.scatter(self.x, self.y, *args, **kwds)

        if 'xlabel' in kwds: ax.set_xlabel(kwds['xlabel'])
        if 'ylabel' in kwds: ax.set_ylabel(kwds['ylabel'])
        if 'title' in kwds: ax.set_title(kwds['title'])
        if 'legend' in kwds: ax.legend(kwds['legend'])

        return

    def find_peaks(self, width_range, resolution, wavelet='ricker', show=False):

        '''
        Automated peak finding algorithm based on Scipy.signal's continuous
        wavelet transformation. CAUTION-STILL IN TESTING

        Currently sensitive to either Gaussian or Lorentzian waveforms only.

        Input:
            width_range: tuple of ints
                Tuple containing lower and upper bounds for possible peak
                widths, in integer number of data points
            resolution: int
                Number of possible widths to sample within bounds defined
                by width_range
            wavelet: str, default='gaussian'
                Type of wavelet to search for in CWT, currently available
                options are:
                    gaussian (default)
                    lorentzian
            show: bool, default=False
                Optionally show identified peaks in new matplotlib window

        Output:
            peak_indices: list
                List of indicies indicating peak centroid locations
            peaks: numpy.array
                Array describing wavelengths and intensities of peak centroids
        '''

        def gaussian_wavelet(d, s):

            '''
            Mathematical expression of Gaussian distribution
            '''

            gaussian = lambda x, m, s: (1/(s*np.sqrt(2*np.pi))) * np.exp(-0.5*(((x-m)/s)**2))
            return gaussian(d, 0, s)

        def lorentzian_wavelet(d, s):

            '''
            Mathematical expression of Cauchy distribution (Lorentzian wavelet)
            '''

            lorentzian = lambda x, m, s: (1/np.pi/s)*((s**2)/(((x - m)**2) + s**2))
            return lorentzian(d, 0, s)

        wavelet_options = {'gaussian':gaussian_wavelet,
                           'lorentzian': lorentzian_wavelet,
                           'ricker': None}

        widths = np.linspace(width_range[0], width_range[1], resolution)
        if not callable(wavelet):
            try:
                wavelet = wavelet_options[wavelet]
            except KeyError:
                raise KeyError('Selected wavelet %s is not a valid option'%(wavelet))

        peak_indices = signal.find_peaks_cwt(self.y, widths, wavelet)

        peak_wavelengths = [self.x[i] for i in peak_indices]
        peak_maxima = [self.y[i] for i in peak_indices]
        n_peaks = len(peak_indices)
        peaks = np.array([peak_wavelengths, peak_maxima])

        if show:
            self.show(append=False)
            for i in range(n_peaks):
                plt.vlines(peak_wavelengths[i], 0, peak_maxima[i], color='r')

        return peak_indices, peaks

    def polynomial_background(self, bounds=None, order=2):
        '''
        Estimates continuum background as n-order polynomial, returns estimated
        background as new Spectrum object.

        Input:
            bounds: iterable
                List or other iterable of tuples defining regions of the spectrum
                manually identified as background regions. For example, if a
                spectrum's intensity follows the background from 10-30 and again
                from 40-70, the bounds would be input as [(10,30),(40,70)]. All
                intensity values within these bounds are used for estimation
                of the continuum background.
            order: int
                Order of polynomial function used for estimating background

        Output:
            bg: Spectrum
                Estimated continuum background
        '''
        spec_type = type(self)
        x = self.x
        y = self.y

        if bounds is None:
            bounds = [(self.x[0], self.x[-1])]

        ii = []
        #Populate list of spectral indicies to use for continuum estimation
        #For each band indicated in bounds, extract indices of all data points
        #within band
        for a, b in bounds:
            ai = find_nearest_member(x, a)
            bi = find_nearest_member(x, b)
            for xi in range(ai, bi+1):
                ii.append(xi)

        #Extract spectral and intensity axis values for all identified indices
        xi = np.array([x[i] for i in ii])
        yi = np.array([y[i] for i in ii])

        #Fit n-order polynomial to extracted x and y data points
        p = np.polyfit(xi, yi, order)
        #Create background spectrum by evaluating fitted polynomial across full
        #spectral axis
        bg = apply_polynomial(x, p)
        bg = spec_type((x, bg))

        return bg

    def huber_background(self, bounds, d=2, order=4):

        '''
        Estimates continuum background as optimized Huber function as described
        by Mazet et al. 2005.

        Input:
            bounds: iterable
                List or other iterable of tuples defining regions of the spectrum
                manually identified as background regions. For example, if a
                spectrum's intensity follows the background from 10-30 and again
                from 40-70, the bounds would be input as [(10,30),(40,70)]. All
                intensity values within these bounds are used for estimation
                of the continuum background.

        Output:
            bg: Spectrum
                Estimated continuum background
        '''

        from scipy.optimize import fmin

        def huber_error(coeff):

            def huber_loss(x, y, d=d):
                difference = np.abs(x-y)
                if difference < d:
                    error = difference**2
                else:
                    error = (2 * difference * d) - d**2
                return error

            poly = apply_polynomial(xi, coeff)
            error = []
            for i, x in enumerate(poly):
                error.append(huber_loss(x, yi[i], d=d))
            return np.sum(error)

        ii = []
        #Populate list of spectral indicies to use for continuum estimation
        #For each band indicated in bounds, extract indices of all data points
        #within band
        for a, b in bounds:
            ai = find_nearest_member(self.x, a)
            bi = find_nearest_member(self.x, b)
            for xi in range(ai, bi+1):
                ii.append(xi)

        #Extract spectral and intensity axis values for all identified indices
        xi = np.array([self.x[i] for i in ii])
        yi = np.array([self.y[i] for i in ii])

        optimized = fmin(huber_error, [0 for i in range(order+1)],disp=1)

        new_spec = self.copy()
        new_spec.y = apply_polynomial(self.x, optimized)

        return new_spec

    def aymmetric_huber_background(self, bounds, d=2, order=4):

        '''
        Estimates continuum background as optimized asymmetric Huber function as
        described by Mazet et al. 2005. Asymmetric loss is proposed as ideal for
        optical spectroscopy where noise is assumed to be Gaussian.

        Input:
            bounds: iterable
                List or other iterable of tuples defining regions of the spectrum
                manually identified as background regions. For example, if a
                spectrum's intensity follows the background from 10-30 and again
                from 40-70, the bounds would be input as [(10,30),(40,70)]. All
                intensity values within these bounds are used for estimation
                of the continuum background.

        Output:
            bg: Spectrum
                Estimated continuum background
        '''

        from scipy.optimize import fmin

        def huber_error(coeff):

            def huber_loss(x, y, d=d):
                difference = x-y
                if difference < d:
                    error = difference**2
                else:
                    error = (2 * difference * d) - d**2
                return error

            poly = apply_polynomial(xi, coeff)
            error = []
            for i, x in enumerate(poly):
                error.append(huber_loss(x, yi[i], d=d))
            return np.sum(error)

        ii = []
        #Populate list of spectral indicies to use for continuum estimation
        #For each band indicated in bounds, extract indices of all data points
        #within band
        for a, b in bounds:
            ai = find_nearest_member(self.x, a)
            bi = find_nearest_member(self.x, b)
            for xi in range(ai, bi+1):
                ii.append(xi)

        #Extract spectral and intensity axis values for all identified indices
        xi = np.array([self.x[i] for i in ii])
        yi = np.array([self.y[i] for i in ii])

        optimized = fmin(huber_error, [0 for i in range(order+1)],disp=1)

        # coefficients = [0 for i in range(order)]
        # n_subsamples = 10

        # for i in range(max_iter):
        #     i+=1
        #     modifier = 1/i / 1000
        #     test_coefficients = np.zeros((n_subsamples,))
        #     for c in coefficients:
        #         test_coefficients = np.vstack((test_coefficients, normal(c, modifier, n_subsamples)))
        #     test_coefficients = test_coefficients[1:]
        #     test_polynomials = [apply_polynomial(self.x, c) for c in test_coefficients.T]
        #     test_errors = []
        #     for t in test_polynomials:
        #         test_errors.append(np.sum(huber_loss(a, self.y[j]) for j, a in enumerate(t)))
        #     coefficients = test_coefficients[:, test_errors.index(np.min(test_errors))]

        new_spec = self.copy()
        new_spec.y = apply_polynomial(self.x, optimized)

        return new_spec

    def savgol_filter(self, window_length=11, order=3):
        '''
        Applies Savitzky-Golay smoothing filter to spectrum. Window width and
        polynomial fitting order may be specified.

        Input:
            window_length: int
                Integer width of sliding window, in array elements. Must be odd
                number
            order: int
                Order of polynomial function used to fit each window.

        Output:
            new_spec: Spectrum
                Filtered spectrum
        '''
        new_spec = self.copy()
        new_spec.y = signal.savgol_filter(new_spec.y, window_length=window_length, polyorder=order)
        return new_spec

    def moving_average_filter(self, window_length=11, edge='ignore'):
        allowed_edge_modes = ['extend','zeros','reflect','wrap','ignore']
        assert edge in allowed_edge_modes, f"Selected edge mode '{edge}' not allowed"
        assert window_length % 2 == 1, 'Window length must be odd'
        filtered = self.copy()
        filtered.y[:] = 0
        for c in range(len(self)):
            window = []
            target_indices = [i for i in range(int(c-((window_length-1)/2)), int(c+((window_length-1)/2)+1))]
            for i, v in enumerate(target_indices):
                if v < 0:
                    if edge == 'ignore':
                        continue
                    elif edge == 'extend':
                        window.append(self.y[0])
                    elif edge == 'zeros':
                        window.append(0)
                    elif edge == 'reflect':
                        window.append(self.y[-v])
                    elif edge == 'wrap':
                        window.append(self.y[v])
                elif v >= len(self):
                    if edge == 'ignore':
                        continue
                    elif edge == 'extend':
                        window.append(self.y[-1])
                    elif edge == 'zeros':
                        window.append(0)
                    elif edge == 'reflect':
                        window.append(self.y[v])
                    elif edge == 'wrap':
                        window.append(self.y[-v])
                else:
                    window.append(self.y[v])
            filtered.y[c] = np.mean(window)
        return filtered

    def nonnegative(self, mode='zero'):
        '''
        Prunes negative values from spectrum by rewriting 0 over all negative
        intensity values.

        Output:
            new_spec: Spectrum
                Spectrum after pruning negative values.
        '''
        new_spec = self.copy()
        if mode == 'zero':
            new_spec.y[np.where(new_spec.y < 0)] = 0
        else:
            raise ValueError("Could not make spectrum nonnegative, mode not implemented")
        return new_spec

    def interpolate(self, new_axis, keep_params=True):
        '''
        Fits spectrum to new x-axis using point-wise linear interpolation.

        Input:
            new_axis: iterable
                New target x-axis values to be remapped to

        Output:
            new_spec: Spectrum
                New spectrum interpolated to new x-axis
        '''

        #Create new empty array of same size as new_axis
        spec_type = type(self)
        interpolated = np.zeros_like(new_axis)
        in_x = self.x
        in_y = self.y

        #For each point along new axis, find nearest 2 values along old axis
        #Apply linear interpolation to find values between existing data points
        for i, x in enumerate(new_axis):

            if x < min(in_x):
                interpolated[i] = 0
                continue
            elif x > max(in_x):
                interpolated[i] = 0
                continue

            i_init = find_nearest_member(in_x, x)
            x_init = in_x[i_init]

            if x_init >= x:
                x_high = x_init
                x_low = in_x[i_init - 1]
                y_high = in_y[i_init]
                y_low = in_y[i_init - 1]

            elif x_init < x:
                x_low = x_init
                x_high = in_x[i_init + 1]
                y_low = in_y[i_init]
                y_high = in_y[i_init + 1]

            fit = np.polyfit((x_low, x_high), (y_low, y_high), 1)
            p = np.poly1d(fit)
            interpolated[i] = p(x)

        new_spectrum = spec_type((new_axis, interpolated))
        if keep_params:
            for k, v in self.parameters.items():
                new_spectrum.parameters[k] = v
        return new_spectrum

    def downsample(self, downsampling_factor, mode='pop', keep_params=True, *args, **kwds):
        '''
        Reduce number of discrete data points by a factor of
        "downsampling_factor". Applies optional smoothing and/or interpolation
        of downsampled data. Filter arguments may be supplied as supplementary
        keywords, otherwise uses default filter settings.
            *pop: no smoothing, simply removes every nth data point
            *savgol: applies savitzky-golay smoothing, then removes every nth data point
            *moving_average: applies moving average smoothing, then removes every nth data point
        '''
        assert mode.lower() in ['pop','savgol','moving_average'], 'Unrecognized smoothing mode!'
        downsampled = self.copy(keep_params)

        if mode.lower() == 'savgol':
            if 'window_length' in kwds:
                window_length = kwds['window_length']
            else:
                window_length = 11
            if 'order' in kwds:
                order = kwds['order']
            else:
                order = 3
            downsampled = downsampled.savgol(window_length, order)

        elif mode.lower() == 'moving_average':
            if 'window_length' in kwds:
                window_length = kwds['window_length']
            else:
                window_length = 11
            if 'edge' in kwds:
                edge = kwds['edge']
            else:
                edge = 'ignore'
            downsampled = downsampled.moving_average_filter(window_length, edge)

        downsampled.x = downsampled.x[::downsampling_factor]
        downsampled.y = downsampled.y[::downsampling_factor]
        return downsampled

    def normalize(self, norm_factor='basepeak', keep_params=False):
        '''
        Normalize spectrum by a variety of methods, specified by keyword
        norm_factor:
            basepeak:   divides all intensities by maximum intensity (default)
            minmax:     first subtracts minimum intensity value from spectrum, then
                        applies basepeak normalization
            area:       normalize such that total integrated spectral intensity = 1.0
            mean:       divide all intensity values by average intensity
            median:     divide all itnensy values by median intensy

        Input:
            norm_factor: str
                Selected normalization mode from list above.

        Output:
            new_spec: Spectrum
                Normalized spectrum

        '''

        def basepeak(spectrum):
            '''
            Divides all intensities by maximum intensity (default)
            '''
            return spectrum / spectrum.abs().max()

        def minmax(spectrum):
            '''
            First subtracts minimum intensity value from spectrum, then
            applies basepeak normalization
            '''
            new_spec = spectrum - spectrum.min()
            return new_spec.normalize(norm_factor='basepeak')

        def area(spectrum):
            '''
            Normalize such that total integrated spectral intensity = 1.0
            '''
            yabs = np.abs(spectrum.y)
            norm_factor = np.trapz(yabs, spectrum.x)
            return spectrum / abs(norm_factor)

        def mean(spectrum):
            #Divide all intensity values by average intensity
            norm_factor = np.mean(spectrum.y)
            return spectrum / abs(norm_factor)

        def median(spectrum):
            #Divide all itnensy values by median intensy
            norm_factor = np.median(spectrum.y)
            return spectrum / abs(norm_factor)

        norm_factor = norm_factor.lower()
        norm_functions = {'basepeak':basepeak,
                          'minmax':minmax,
                          'area':area,
                          'mean':mean,
                          'median':median}

        #Check to make sure selected normalization mode is a valid option
        if norm_factor in norm_functions:
            #Select indicated normalization function from dict of options
            f = norm_functions[norm_factor]
        else:
            raise KeyError('Seleted normalization mode %s is not a valid option'%(norm_factor))
        normalized = self.copy(keep_params=keep_params)
        normalized = f(normalized)
        return normalized

    def derivative(self, keep_params=True):

        self_type = type(self)
        d = np.gradient(self.y, self.x)
        derivative_spectrum = Spectrum((self.x, self.y))
        derivative_spectrum.parameters = self.parameters
        derivative_spectrum.y = d
        return derivative_spectrum

    def integrate(self, min=None, max=None, isopeak=False):
        '''
        Calculate integrated spectral intensity within region bounded by min and
        max.

        Input:
            min: int or float
                Lower bound of extracted slice, in units/values of spectral axis
            max: int or float
                Upper bound of extracted slice, in units/values of spectral axis

        Output:
            integral: float
                Integrated spectral intensity along selected band
        '''
        if min is None:
            min = self.x.min()
        if max is None:
            max = self.x.max()
        window = self.slice(min, max)

        '''
        isopeak flag enables 1st order baseline subtraction using leftmost and
        rightmost points in window to estimate baseline.

        Described by
        Gierlinger N, Schwanninger M. Plant Physiol. 2006;140(4):1246-1254.
        '''
        if isopeak:
            xi = [window.x[0],window.x[-1]]
            yi = [window.y[0], window.y[-1]]
            p = np.polyfit(xi, yi, 1)
            isobackground = apply_polynomial(window.x, p)
            window.y = np.subtract(window.y, isobackground)

        integral = np.trapz(window.y, window.x)
        return integral
    
    def cumsum(self):
        newspec = self.copy(keep_params=False)
        newspec.y = np.cumsum(self.y)
        return newspec

    def find_centroid(self, low, high):
        """
        Finds center-of-mass centroid of a peak bounded by low and high on the
        spectral axis.
        """

        #Extract slice of Spectrum to be sampled
        window = self.slice(low, high)

        #Create list to store differences in integrated weights
        differences = []

        #Split slice at all possible spectral values.
        for i in range(1, len(window)):
            left_weight = sum(window.y[:i])
            right_weight = sum(window.y[i:])
            #Measure difference in weights of each segment, store to list
            differences.append(abs(left_weight - right_weight))

        #Identify point at which difference in weights is minimized
        centroid = differences.index(min(differences))
        #Return spectral axis value corresponding to the identified centroid
        centroid = window.x[centroid]
        return centroid

    def fft(self):
        out = np.fft.rfft(self.y)
        return out

    @staticmethod
    def ifft(ft, x_ax=None):
        ift = np.fft.irfft(ft)
        if x_ax is None:
            x_ax = [i for i in range(len(ift))]
        spectrum = Spectrum((x_ax, ift))
        return spectrum

    def fft_filter(self, max_freq=15):
        spec_type = type(self)
        out = self.fft()
        out[max_freq:] = 0
        inverse = spec_type.ifft(out, self.x[:-1])
        for k, v in self.parameters.items():
            inverse.parameters[k] = v
        return inverse
    
    def LCF(self, components, error='rmse', fit_space=None, tol=1E-5,
            nonnegative=True, max_iter=200):
        
        #Define and check for valid fitting space
        fit_spaces = [None,'norm','deriv','deriv2']
        if fit_space not in fit_spaces:
            raise ValueError('Invalid selection for argument fit_space')
            
        #Initialize constraints
        constraints = []
        #Create working copies of data and standards
        data = self.copy()
        #Interpolate components to same data range as 'data'
        for i, c in enumerate(components):
            components[i] = c.interpolate(data.x)
        raw_components = components.copy()
        
        def weight_error(weights):
            '''
            Calculates fitting error from global and component scaling weights.

            Parameters
            ----------
            weights : TYPE
                DESCRIPTION.

            Returns
            -------
            err : TYPE
                DESCRIPTION.

            '''
            weighted = components[0]*weights[1]
            for i, c in enumerate(components[1:]):
                i+=2
                weighted += c*weights[i]
            weighted = weighted * weights[0]
            err = RMSE(data, weighted)
            return err
        
        
        #Calculate 1st and 2nd derivative if needed
        if fit_space=='deriv' or fit_space=='deriv2':
            data = data.derivative()
            components = components.derivative()
            if fit_space == 'deriv2':
                data = data.derivative()
                components = components.derivative()
            
        #Initialize fitting weights
        init_weights = [1]
        init_weights += [1/len(components) for c in components]
        
        #Apply constraints
        if nonnegative:
            A = np.zeros((len(init_weights), len(init_weights)))
            for i in range(len(init_weights)):
                A[i,i] = 1
            constraint = LinearConstraint(A, lb=0, ub=np.inf)
            constraints.append(constraint)
        constraints = tuple(constraints)
        
        #Optimize fitting
        opt_result = minimize(weight_error, init_weights, tol=tol, constraints=constraints,
                              options={'maxiter':max_iter})
        
        #Extract and report fit parameters
        params = opt_result['x']
        scale = params[0]
        weights = params[1:]
        fitted = raw_components[0] * weights[0]
        for i, c in enumerate(raw_components[1:]):
            i+=1
            w = weights[i]
            fitted += c*w
        return opt_result, fitted

class Series:

    '''
    Container class for sets of related spectra. Methods for handling groups of
    spectra.
    '''

    def __init__(self, iterable=None, parameters=None):
        '''
        Creates new Series object. If an interable of Spectrum objects is
        provided, populates new Series with provided Spectra.
        '''
        self.spectra = None
        if iterable is not None and len(iterable) > 0:
            for item in iterable:
                #!!! Add check to ensure all items are valid Spectrum or derivative objects?
                self.append(item)
        self.parameters = {}
        if parameters is not None:
            for k, v in parameters.items():
                self.parameters[k] = v
        return

    def __add__(self, other):
        '''
        If other is a single Spectrum, adds the y axis intensity of the new
        spectrum to all spectra in the Series.

        If other is another Series object, appends all spectra in other to
        self.spectra.
        '''
        new_series = self._copy_empty_(keep_params=True)

        if issubclass(type(other), Series):
            for spectrum in self:
                new_series.append(spectrum)
            for spectrum in other:
                new_series.append(spectrum)

        else:
            for spectrum in self:
                new_series.append(spectrum + other)

        return new_series

    def __sub__(self, other):
        'Subtract same Spectrum or value from all Spectra in Series'
        new_series = self._copy_empty_()
        for spectrum in self:
            new_series.append(spectrum - other)
        return new_series

    def __mul__ (self, other):
        '''
        Multiply all Spectra in Series by same Spectrum or value
        '''
        new_series = self._copy_empty_()
        for spectrum in self:
            new_series.append(spectrum * other)
        return new_series

    def __truediv__(self, other):
        '''
        Divide all Spectra in Series by same Spectrum or value
        '''
        new_series = self._copy_empty_()
        for spectrum in self:
            new_series.append(spectrum / other)
        return new_series

    def __iter__(self):
        '''
        Implement iteration of Series object, iterates over self.spectra
        '''
        self.iter_count = 0
        return self

    def __next__(self):
        '''
        Return next element during iteration
        '''
        if self.iter_count < len(self.spectra):
            result = self.spectra[self.iter_count]
            self.iter_count += 1
            return result
        else:
            raise StopIteration

    def __len__(self):
        if self.spectra is None: return 0
        else: return len(self.spectra)

    def __getitem__(self, i):
        '''
        Implement indexing of Series object, returns ith element of self.spectra
        '''
        item = self.spectra[i]
        if issubclass(type(item), Spectrum):
            pass
        else:
            item = Series([i for i in item])
        return item

    def __setitem__(self, key, value):
        self.spectra[key] = value
        return

    def __delitem__(self, key):
        del self.spectra[key]

    def _decompose_(self):
        '''
        Decompose Series into list of decomposed Spectra
        '''
        return [s._decompose_() for s in self]

    def _copy_empty_(self, keep_params=False):
        '''
        Creates duplicate copy of self, then reinitializes spectra attribute
        of the newly created copy (ie. creates new empty Series of same type as
        parent)
        '''
        series_type = type(self)
        new_series = series_type()
        return new_series

    def min(self):
        '''
        Finds minimum intensity value in Series
        '''
        minimum = None
        for spectrum in self:

            if minimum is None:
                minimum = min(spectrum)
            else:
                spec_min = min(spectrum)
                if spec_min < minimum: minimum = spec_min

        return minimum

    def max(self):
        '''
        Finds maximum intensity value in Series
        '''
        maximum = None
        for spectrum in self:

            if maximum is None:
                maximum = max(spectrum)
            else:
                spec_max = max(spectrum)
                if spec_max > maximum: maximum = spec_max

        return maximum

    def copy(self, keep_params=False):
        '''
        Creates a duplicate copy of self
        '''
        series_type = type(self)
        new_series = series_type()
        for spectrum in self:
            spec_type = type(spectrum)
            new_spec = spec_type((spectrum.x, spectrum.y))
            if keep_params:
                for k, v in spectrum.parameters.items():
                    new_spec.parameters[k]=v
            new_series.append(new_spec)
        return new_series

    def slice(self, a, b):
        '''
        Applies Spectrum.slice to all Spectra in Series
        '''
        new_series = self._copy_empty_()
        for spectrum in self:
            new_series.append(spectrum.slice(a,b))
        return new_series

    def append(self, other):
        '''
        Adds new spectrum to the end of self.spectra
        '''
        if self.spectra is None:
            self.spectra = [other]
        else:
            self.spectra.append(other)
        return

    def imprint_params(self, parameters=None):
        for spectrum in self:
            if parameters is None:
                for k, v in self.parameters.items():
                    spectrum.parameters[k] = v
            else:
                for k in parameters.keys():
                    spectrum.parameters[k] = self.parameters[k]
        return

    def remove(self, index):
        del self.spectra[index]
        return

    def sum(self):
        '''
        Computes sum Spectrum from all Spectra in Series
        '''
        new_spec = self.spectra[0].copy(keep_params=False)
        new_spec.y[:] = 0
        for spectrum in self.spectra:
            new_spec += spectrum
        return new_spec
    
    def log(self, base=10):
        new_series = self._copy_empty_()
        for spectrum in self:
            new_series.append(spectrum.log(base=base))
        return new_series

    def average(self):
        '''
        Computes average Spectrum from all Spectra in Series
        '''
        sum_spectrum = self.sum()
        return sum_spectrum / len(self)

    def derivative(self, order=1):
        '''
        Calculates the derivative of all spectra in the input series.
        
        Returns
        -------
        derivatives : Series
            Derivatives of all spectra in input series.

        '''
        
        derivatives = self.copy()
        for j in range(order):
            for i, s in enumerate(self):
                derivatives[i] = s.derivative()
        return derivatives
        
    def show(self, *args, **kwds):
        '''
        Prints all spectra in series to new Matplotlib figure.
        '''
        if 'figsize' in kwds:
            plt.figure(figsize=kwds['figsize'])
        else:
            plt.figure()
        if 'cmap' in kwds:
            cmap=kwds['cmap']
        else:
            cmap='viridis'
        vectors = np.vstack([s.y for s in self])
        l, s = (len(self), self[0].x[-1]-self[0].x[0])
        im = plt.imshow(vectors, vmin=0, vmax=np.std(vectors)*5, aspect=s/l,
                        extent=[self[0].x[0], self[0].x[-1], len(self), 0], cmap=cmap)
        plt.colorbar()
        if 'xlabel' in kwds: plt.xlabel(kwds['xlabel'])
        if 'ylabel' in kwds: plt.ylabel(kwds['ylabel'])
        if 'title' in kwds: plt.title(kwds['title'])
        if 'legend' in kwds: plt.legend(kwds['legend'])
        return im

    def set(self, bounds, value=0):
        '''
        Calls Spectrum.set() for all members of self, returns copy of self
        '''
        self_type = type(self)
        return self_type([s.set(bounds, value) for s in self])

    def interpolate(self, new_axis, keep_params=True):
        interpolated = self._copy_empty_()
        for spectrum in self:
            interpolated.append(spectrum.interpolate(new_axis, keep_params))
        return interpolated

    def PCA(self, n_components=None, pass_meta=False):
        '''
        Applies principal component analysis to all spectra in Series,
        wrapper for sklearn.decomposition.PCA
        '''
        from sklearn.decomposition import PCA
        
        try:
            flattened = self.flatten()
        except AttributeError:
            flattened = self

        input_matrix = np.vstack([s.y for s in flattened])

        if n_components is None:
            n_components = len(self)

        model = PCA(n_components)
        X = model.fit_transform(input_matrix)
        Xr = model.inverse_transform(X)
        new_series = flattened.copy()
        for i, spectrum in enumerate(new_series):
            spectrum.y[:] = Xr[i, :]
            spectrum.parameters['PCA_weights'] = X[i]
        
        x_ax = self[0].x
        components = Series()
        for c in model.components_:
            components.append(Spectrum((x_ax, c)))
            
        return model, X, components, new_series

    def NMF(self, n_components, init=None, solver='cd', beta_loss='frobenius',
            tol=1e-4, max_iter=20000, random_state=None, l1_ratio=0,
            verbose=False, shuffle=False, pass_meta=False, component_filter=[]):
        '''
        Applies non-negative matrix factorization to all spectra in Series,
        wrapper for sklearn.decomposition.NMF
        '''
        from sklearn.decomposition import NMF

        input_matrix = np.vstack([s.y for s in self])
        model = NMF(n_components, init=init, solver=solver, beta_loss=beta_loss,
                    tol=tol, max_iter=max_iter, random_state=random_state,
                    l1_ratio=l1_ratio, verbose=verbose,
                    shuffle=shuffle)
        raw_weights = model.fit_transform(input_matrix)
        filtered_weights = np.array(raw_weights)
        for c in component_filter:
            filtered_weights[:,c] = 0
        Xr = model.inverse_transform(filtered_weights)
        
        reconstructed = self.copy()
                    
        for i, spectrum in enumerate(reconstructed):
            spectrum.y[:] = Xr[i, :]
            spectrum.parameters['NMF_weights'] = raw_weights[i]

            if pass_meta:
                for k, v in self[i].parameters.items():
                    spectrum.parameters[k] = v

        x_ax = self[0].x
        components = Series()
        for c in model.components_:
            components.append(Spectrum((x_ax, c)))

        return model, raw_weights, components, reconstructed

    def VCA(self, n_components, pass_meta=False):
        #VCA Function (Adapted from https://github.com/Laadr/VCA/blob/master/VCA.py)
        import sys
        import scipy as sp
        import scipy.linalg as splin

        #############################################
        # Internal functions
        #############################################

        def estimate_snr(Y,r_m,x):

          [L, N] = Y.shape           # L number of bands (channels), N number of pixels
          [p, N] = x.shape           # p number of endmembers (reduced dimension)

          P_y     = sp.sum(Y**2)/float(N)
          P_x     = sp.sum(x**2)/float(N) + sp.sum(r_m**2)
          snr_est = 10*sp.log10( (P_x - p/L*P_y)/(P_y - P_x) )

          return snr_est



        def vca(Y, R, verbose=True, snr_input=0):

        # Vertex Component Analysis
        #
        # Ae, indice, Yp = vca(Y,R,verbose = True,snr_input = 0)
        #
        # ------- Input variables -------------
        #  Y - matrix with dimensions L(channels) x N(pixels)
        #      each pixel is a linear mixture of R endmembers
        #      signatures Y = M x s, where s = gamma x alfa
        #      gamma is a illumination perturbation factor and
        #      alfa are the abundance fractions of each endmember.
        #  R - positive integer number of endmembers in the scene
        #
        # ------- Output variables -----------
        # Ae     - estimated mixing matrix (endmembers signatures)
        # indice - pixels that were chosen to be the most pure
        # Yp     - Data matrix Y projected.
        #
        # ------- Optional parameters---------
        # snr_input - (float) signal to noise ratio (dB)
        # v         - [True | False]
        # ------------------------------------
        #
        # Author: Adrien Lagrange (adrien.lagrange@enseeiht.fr)
        # This code is a translation of a matlab code provided by
        # Jose Nascimento (zen@isel.pt) and Jose Bioucas Dias (bioucas@lx.it.pt)
        # available at http://www.lx.it.pt/~bioucas/code.htm under a non-specified Copyright (c)
        # Translation of last version at 22-February-2018 (Matlab version 2.1 (7-May-2004))
        #
        # more details on:
        # Jose M. P. Nascimento and Jose M. B. Dias
        # "Vertex Component Analysis: A Fast Algorithm to Unmix Hyperspectral Data"
        # submited to IEEE Trans. Geosci. Remote Sensing, vol. .., no. .., pp. .-., 2004


          #############################################
          # Initializations
          #############################################
          if len(Y.shape) != 2:
            sys.exit('Input data must be of size L (number of bands i.e. channels) by N (number of pixels)')

          [L, N] = Y.shape   # L number of bands (channels), N number of pixels

          R = int(R)
          if (R < 0 or R > L):
            sys.exit('ENDMEMBER parameter must be integer between 1 and L')

          #############################################
          # SNR Estimates
          #############################################

          if snr_input == 0:
            y_m = sp.mean(Y, axis=1, keepdims=True)
            Y_o = Y - y_m           # data with zero-mean
            Ud  = splin.svd(sp.dot(Y_o, Y_o.T) / float(N))[0][:, :R]  # computes the R-projection matrix
            x_p = sp.dot(Ud.T, Y_o)                 # project the zero-mean data onto p-subspace

            SNR = estimate_snr(Y, y_m, x_p);

            if verbose:
              print("SNR estimated = {}[dB]".format(SNR))
          else:
            SNR = snr_input
            if verbose:
              print("input SNR = {}[dB]\n".format(SNR))

          SNR_th = 15 + 10 * sp.log10(R)

          #############################################
          # Choosing Projective Projection or
          #          projection to p-1 subspace
          #############################################

          if SNR < SNR_th:
            if verbose:
              print("... Select proj. to R-1")

              d = R - 1
              if snr_input == 0: # it means that the projection is already computed
                Ud = Ud[:, :d]
              else:
                y_m = sp.mean(Y, axis=1, keepdims=True)
                Y_o = Y - y_m  # data with zero-mean

                Ud  = splin.svd(sp.dot(Y_o,Y_o.T) / float(N))[0][:, :d]  # computes the p-projection matrix
                x_p =  sp.dot(Ud.T, Y_o)                 # project thezeros mean data onto p-subspace

              Yp =  sp.dot(Ud, x_p[:d, :]) + y_m      # again in dimension L

              x = x_p[:d, :] #  x_p =  Ud.T * Y_o is on a R-dim subspace
              c = sp.amax(sp.sum(x ** 2, axis=0)) ** 0.5
              y = sp.vstack(( x, c * sp.ones((1, N)) ))
          else:
            if verbose:
              print("... Select the projective proj.")

            d = R
            Ud  = splin.svd(sp.dot(Y, Y.T) / float(N))[0][:, :d] # computes the p-projection matrix

            x_p = sp.dot(Ud.T, Y)
            Yp =  sp.dot(Ud, x_p[:d, :])      # again in dimension L (note that x_p has no null mean)

            x =  sp.dot(Ud.T, Y)
            u = sp.mean(x, axis=1, keepdims=True)        #equivalent to  u = Ud.T * r_m
            y =  x / sp.dot(u.T, x)


          #############################################
          # VCA algorithm
          #############################################

          indice = sp.zeros((R), dtype=int)
          A = sp.zeros((R, R))
          A[-1, 0] = 1

          for i in range(R):
            w = sp.random.rand(R, 1);
            f = w - sp.dot(A, sp.dot(splin.pinv(A), w))
            f = f / splin.norm(f)

            v = sp.dot(f.T, y)

            indice[i] = sp.argmax(sp.absolute(v))
            A[:, i] = y[:, indice[i]]        # same as x(:,indice(i))

          Ae = Yp[:, indice]

          return Ae, indice, x_p, Yp

        x_ax = self.spectra[0].x
        input_vectors = np.vstack([s.y for s in self.spectra])
        input_vectors = input_vectors.T
        Ae, indices, weights, reconstructed = vca(input_vectors, n_components)
        endmembers = Series([self.spectra[i] for i in indices])
        reconstructed = reconstructed.T
        reconstructed = Series([Spectrum((x_ax, spec)) for spec in reconstructed])
        Ae = Series([Spectrum((x_ax, spec)) for spec in Ae.T])
        return Ae, endmembers, weights, reconstructed

    def KMeans(self, n_classes, pass_meta=False):

        from sklearn.cluster import KMeans

        model = KMeans(n_classes)
        vectors = np.vstack(s[1] for s in self._decompose_())
        model.train(vectors)
        labels = model.predict(vectors)
        for i, s in enumerate(self):
            s.parameters['kmeans_label'] = labels[i]
        return model, labels
    
    def UMAP(self, n_neighbors, n_dim, pass_meta=False, min_dist=0,
             *args, **kwds):
        
        try:
            from umap import UMAP
        except NameError:
            print('UMAP functionality requires additional umap dependency')
            print('Install using pip install umap-learn')
            raise NameError
        
        model = UMAP(n_neighbors=n_neighbors, n_components=n_dim, random_state=1312, *args, **kwds)
        train_y = np.array([spectrum.y for spectrum in self])
        X = model.fit_transform(train_y)
        return X, model

    def nonnegative(self, mode='zero'):
        '''
        Applies Spectrum.nonnegative() to all Spectra in Series, see
        Spectrum.nonnegative for more details.
        '''
        new_series = self._copy_empty_()
        for spectrum in self:
            new_series.append(spectrum.nonnegative(mode=mode))
        return new_series

    def savgol_filter(self, window_length=11, order=3):
        '''
        Applies Spectrum.savgol_filter() to all Spectra in Series, see
        Spectrum.savgol_filter for more details.
        '''
        new_series = self._copy_empty_()
        for s in self:
            new_series.append(s.savgol_filter(window_length=window_length, order=order))
        return new_series

    def moving_average_filter(self, window_length, edge='ignore', keep_params=True):
        new_series = self._copy_empty_()
        for s in self:
            new_series.append(s.moving_average_filter(window_length=window_length,edge=edge,keep_params=keep_params))
        return new_series

    def normalize(self, norm_factor='basepeak', keep_params=False):
        '''
        Applies Spectrum.normalize() to all Spectra in Series, see
        Spectrum.normalize for more details.
        '''
        new_series = self._copy_empty_()
        for spectrum in self:
            new_series.append(spectrum.normalize(norm_factor, keep_params))
        return new_series

    def integrate(self, low, high):
        intensities = np.array([spectrum.integrate(low, high) for spectrum in self])
        return intensities

    def scree(self, n_components=None, show=False, model='PCA'):
        #Perform pseudo-svd
        model = model.upper()
        allowed_models = set(['PCA', 'NMF'])
        if model not in allowed_models:
            raise ValueError(f'Selected model {model} is not recognized')
        if n_components is None:
            if model == 'NMF':
                n_components = min([10, len(self.spectra)])
            else:
                n_components = min(len(self), len(self[0]))

        if model == 'PCA':
            pca, X, C, Xr = self.PCA(n_components)
            y = np.cumsum(pca.explained_variance_ratio_)
        elif model == 'NMF':
            randomstate = 1312
            y = []
            for i in range(1,n_components):
                nmf, X, C, Xr = self.nonnegative().NMF(i, random_state=randomstate, max_iter=99999999999999999)
                errors = [chiSquared(s, Xr[j]) for j, s in enumerate(self.spectra)]
                y.append(np.sum(errors))

        if show:
            x = [i+1 for i in range(n_components)]
            plt.scatter(x, y*100)
            plt.title('Scree Plot')
            plt.xlabel('# PCA Components')
            plt.ylabel('Cumulative Explained Variance (%)')
        return y

    def save(self, fname):
        spec_type = str(type(self))
        decomposed = self._decompose_()
        n_spectra = len(self)
        spectral_length = len(self[0].x)
        with hf.File(fname,'w') as f:
            spectra_dset = f.create_dataset('spectra', (n_spectra, 2, spectral_length))
            for i, (x, y, p) in enumerate(decomposed):
                spectra_dset[i,0] = x
                spectra_dset[i,1] = y
            spectra_dset.attrs['type'] = spec_type
        return

    @staticmethod
    def load(fname):
        from .series import RamanSeries, PhotoSeries
        from .spectra import RamanSpectrum, PhotoSpectrum
        type_mapping = {
            "<class 'Instruments.core.Series'>": (Spectrum, Series),
            "<class 'Instruments.series.RamanSeries'>": (RamanSpectrum, RamanSeries),
            "<class 'Instruments.series.PhotoSeries'>": (PhotoSpectrum, PhotoSeries)
        }
        with hf.File(fname,'r') as f:
            spectra_dset = f['spectra']
            decomposed = spectra_dset[:]
            spec_type = spectra_dset.attrs['type']
        spec_type, series_type = type_mapping[spec_type]
        spectra = []
        for s in decomposed:
            x = s[0]
            y = s[1]
            spectrum = spec_type((x, y))
            spectra.append(spectrum)
        spectra = series_type(spectra)
        return spectra

# Utility functions

def cat(spectra):
    new_x = np.hstack([spec.x for spec in spectra])
    new_y = np.hstack([spec.y for spec in spectra])
    new_y = proxy_sort(new_x, new_y)
    new_x = proxy_sort(new_x, new_x)
    newspec = Spectrum((new_x, new_y))
    return newspec

def RMSE(spectrum1, spectrum2):
    '''
    Calculate the root-mean square error between two spectra
    '''

    def rmse(measured, estimated):
        measured = np.array(measured)
        estimated = np.array(estimated)
        error = measured - estimated
        error = np.square(error)
        mse = np.mean(error)
        return np.sqrt(mse)

    # _assert_compatibility_(spectrum1, spectrum2)
    return rmse(spectrum1.y, spectrum2.y)

def _assert_compatibility_(spectrum1, spectrum2):
    '''
    Tests spectrum compatiblity with series of assertions.
    '''
    assert len(spectrum1) == len(spectrum2), "Input spectra must have the same length!"
    assert type(spectrum1) is type(spectrum2), "Input spectra must be of the same type!"
    return

def chiSquared(spectrum1, spectrum2):
    '''
    Calculates the chi^2 difference metric for two input spectra.
    '''
    c2 = lambda v1, v2 : (v1-v2) / v2
    _assert_compatibility_(spectrum1, spectrum2)
    spectype = type(spectrum1)
    diff = np.zeros_like(spectrum1)
    for i, v1 in enumerate(spectrum1):
        v2 = spectrum2.y[i]
        diff[i] = c2(v1, v2)
    chisquared = diff.sum()
    return chisquared

def SpectralCorrelation(spectrum1, spectrum2):
    '''
    Calculate the correlation between two spectra.
    '''
    _assert_compatibility_(spectrum1,spectrum2)
    return np.corrcoef(spectrum1.y, spectrum2.y)[0,1]

def apply_polynomial(x, c):
    '''
    Applies nth order polynomial to input array x. n is equal to len(c) - 1.

    when c = (1, -2, 3), function is equivalent to:
        f(x) = 3*x**2 - 2*x + 1

    Input:
    --------
        x : array-like
            data to be evaluated with polynomial
        c : array-like
            polynomial coefficients in ascending polynomial order
    '''
    import numpy as np
    x = np.asarray(x)
    y = np.empty_like(x, dtype=np.float64)
    for i, v in enumerate(x):
        y_i = 0
        for idx, coeff in enumerate(np.flip(c)):
            y_i += (v**idx)*coeff
        y[i] = y_i
    return y

def find_nearest_member(container, query, truncate=False):
    '''
    Finds the member of a container whose value is nearest to query. Returns
    index of nearest value within container. Intended to be used when
    list.index(query) is, for whatever reason, not a viable option for locating
    the desired value within the container.

    Input:
    --------
    container : container variable (eg list, tuple, set, Numpy array)
        The container to be searched by the function
    query : number (eg int or float)
        Value to be searched for within container

    Output:
    --------
    mindex : int
        Index of item in container whose value most nearly matches query
    '''
    c_min = min(container)
    c_max = max(container)
    if truncate:
        if query > c_max or query < c_min:
            raise ValueError('Query is not within range of container.')
    try:
        diffs = abs(container - query)
    except:
        diffs = []
        for entry in container:
            difference = entry - query
            diffs.append(abs(difference))
    minimum = min(diffs)
    mindex = list(diffs).index(minimum)
    return mindex

def proxy_sort(template, data):
    import numpy as np
    order = np.argsort(template)
    sorted_data = [data[i] for i in order]
    return sorted_data

def _migrate_params_(source, destination):
    for k, v in source.parameters.items():
        destination.parameters[k] = v
    return
