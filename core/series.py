# -*- coding: utf-8 -*-
"""
Created on Wed May 27 11:54:44 2020

@author: tyler
"""

from .core import Series
from .spectra import *
import numpy as np
import h5py as h5

class PhotoSeries(Series):

    def __init__(self, iterable=None):
        Series.__init__(self, iterable)
        return

    def calibrate(self, concentrations, target_wavelength='max', plot=False):
        if target_wavelength == 'max':
            cal_points = [max(spec.y) for spec in self.spectra]
            cal_wavelengths = []
            for i, v in enumerate(cal_points):
                spec_x = list(self.spectra[i].x)
                spec_y = list(self.spectra[i].y)
                ind = spec_y.index(v)
                cal_wavelengths.append(spec_x[ind])
            print('Maxima: ', cal_wavelengths)
        else:
            wl_indices = [find_nearest_member(spectrum.x, target_wavelength) for spectrum in self.spectra]
            cal_points = []
            for i, spectrum in enumerate(self.spectra):
                ii = wl_indices[i]
                cal_points.append(spectrum.y[ii])
        concentrations.append(0)
        cal_points.append(0)
        p = np.polyfit(concentrations, cal_points, 1)
        if plot:
            plt.scatter(concentrations, cal_points)
            xx = [min(concentrations), max(concentrations)]
            yy = apply_polynomial(xx, p)
            plt.plot(xx, yy, color='r')
            plt.title('Calibration Curve')
            plt.xlabel('Concentration (mM)')
            plt.ylabel('Absorbance (arb)')
        r = np.corrcoef(concentrations, cal_points)[0, 1]
        R2 = r**2

        return p, R2

    def corr_curve(self, concentrations):
        yy = []
        for spectrum in self.spectra:
            yy.append(spectrum.y)
        yy = np.array(yy)
        rs = []
        for i in range(631):
            yi = yy[:, i]
            rs.append(np.corrcoef(concentrations, yi)[0, 1]**2)
        plt.plot(spectrum.x, rs)
        plt.xlabel('Wavelength (nm)')
        plt.ylabel('R-squared')
        r_max = max(rs)
        x_max = spectrum.x[rs.index(r_max)]
        print('Maximum R-squared of', r_max, 'at', x_max, 'nm')
        return

class RamanSeries(PhotoSeries):

    def __init__(self, iterable=None):
        PhotoSeries.__init__(self, iterable)
        return

    def __len__(self):
        if self.spectra is None: return 0
        else: return len(self.spectra)

class ConfocalSeries(RamanSeries):

    def __init__(self, spec, intensity):

        #Compatibility checks
        dimensions = len(intensity.shape)-1
        if dimensions == 2:
            spec_type = type(intensity[0,0])
        elif dimensions == 3:
            spec_type = type(intensity[0,0,0])
        else:
            raise ValueError('Incompatible shape: input array must have either 2 or 3 axes')
        self.spec_axis = np.array(spec)

        if spec_type is Spectrum or issubclass(spec_type, Spectrum):
            self.shape = list(intensity.shape)
            self.shape.append(len(self.spec_axis))
            self.shape = tuple(self.shape)
        else:
            if spec_type is not np.ndarray:
                raise TypeError('Input intensity object must be Numpy array')
            if len(intensity.shape) != 4:
                raise ValueError('Input intensity array must be 4-dimensional')
            if len(spec) != intensity.shape[3]:
                raise ValueError('Spectral axis and intensity axis must have same length.')
            self.shape = tuple(intensity.shape)

        #Infer dimensionality from length of shape attribute
        spacial_axes = list(self.shape)
        spectral_axis = spacial_axes.pop()
        spacial_axes = tuple(spacial_axes)

        if spec_type is Spectrum or issubclass(spec_type, Spectrum):
            self.spectra = intensity
        else:
            self.spectra = np.empty(spacial_axes, dtype=RamanSpectrum)
            dummy_array = np.zeros(spacial_axes)
            hits = np.where(dummy_array==0)
            for i in range(len(hits[0])):
                coord_tuple = tuple([np.array(v) for v in np.array(hits).T[i]])
                new_spectrum = RamanSpectrum((self.spec_axis, intensity[coord_tuple]))
                new_spectrum.parameters['position'] = tuple([c.reshape(1)[0] for c in coord_tuple])
                self.spectra[coord_tuple] = new_spectrum
        return

    def _decompose_(self):
        decomposed_intensities = np.zeros(self.shape)
        for spectrum in self:
            x, y, z = spectrum.parameters['position']
            decomposed_intensities[x, y, z, :] = spectrum.y
        decomposed_spectral_axis = self.spec_axis
        return (decomposed_spectral_axis, decomposed_intensities)

    def __iter__(self):
        self._iter_count_ = 0
        return self

    def __len__(self):
        geometric_sum = 1
        for i in self.shape[:-1]:
            geometric_sum = geometric_sum * i
        return geometric_sum

    def __next__(self):
        if self._iter_count_ < len(self):
            result = self.spectra.flatten()[self._iter_count_]
            self._iter_count_ += 1
            return result
        else:
            raise StopIteration

    def __getitem__(self, input_tuple):

        dropped_axes = []
        selection_shape = list(self.shape)
        for i, item in enumerate(input_tuple):
            if type(item) is not slice:
                selection_shape[i] = 1

        selection_tuple = list(input_tuple)
        selection = self.spectra[input_tuple]

        dtype = type(selection)

        if dtype is Spectrum or issubclass(dtype, Spectrum):
            return selection
        else:
            return ConfocalSeries(self.spec_axis, selection)

    def sum(self):
        flattened = self.flatten()
        return flattened.sum()

    def average(self):
        sum_spectrum = self.sum()
        return sum_spectrum / len(self)

    def flatten(self, keep_params=True):
        x, y, z, s = self.shape
        flattened = RamanSeries()
        for xi in range(x):
            for yi in range(y):
                for zi in range(z):
                    spectrum = self.spectra[xi, yi, zi].copy(keep_params=keep_params)
                    flattened.append(spectrum)
        return flattened

    def copy(self):
        spec, intensity = self._decompose_()
        return ConfocalSeries(spec, intensity)

    def integrate(self, bounds):
        a, b = bounds
        xx, yy, zz, ss = self.shape
        integrated_intensities = np.zeros((xx, yy, zz), dtype=float)
        for xi in range(xx):
            for yi in range(yy):
                for zi in range(zz):
                    spectrum = self[xi, yi, zi]
                    integrated_intensities[xi, yi, zi] = spectrum.integrate(a, b)
        return integrated_intensities

    def nonnegative(self):
        new_series = self.copy()
        x, y, z, s = self.shape
        for i in range(x):
            for j in range(y):
                for k in range(z):
                    new_series[i,j,k] = self.spectra[i,j,k].copy().nonnegative()
        return new_series

    def show(self, mode='gradient', *args, **kwds):

        if mode == 'gradient':
            self.flatten().show()

        elif mode == 'panel':
            axes = args[0]
            depth = args[1]
            wavenumbers = args[2]
            if axes == (0,1):
                panel = self[:,:,depth]
            elif axes == (0, 2):
                panel = self[:,depth,:]
            elif axes == (1, 2):
                panel = self[depth,:,:]

            cmap = 'viridis'
            if 'cmap' in kwds:
                cmap = kwds['cmap']

            x, y = (panel.shape[axes[0]], panel.shape[axes[1]])
            intensities = panel.integrate((wavenumbers[0], wavenumbers[1]))
            intensities = intensities.reshape((x, y))
            plt.imshow(intensities, cmap=cmap)
        return

    def save(self, filename):
        '''
        Saves ConfocalSeries as a .txt file. Requires position information for
        each spectrum.
        '''
        flat = self.flatten()
        x_ax = flat[0].x
        with open(fname, 'w') as f:
            line = ',,,'
            for xi in x_ax:
                line += f'{xi:.06},'
            line = line[:-1] + '\n'
            f.write(line)
            for s in flat:
                x, y, z = s.parameters['position']
                line = f'{x},{y},{z},'
                for yi in s.y:
                    line += f'{yi:.06},'
                line = line[:-1] + '\n'
                f.write(line)
        return

class XRDSeries(Series):

    def __init__(self):
        Series.__init__(self)
        return

class CVSeries(Series):

    def __init__(self, cd_list=None):
        Series.__init__(self)
        if cd_list is not None:
            for cycle in cd_list:
                self.append(CVSpectrum(cycle))
        return

    def show(self, append=False):
        if not append:
            plt.figure()
        for spectrum in self.spectra:
            spectrum.show()
        return

    def calc_specific_energy(self, active_mass):
        specific_energies = []
        for spectrum in self:
            specific_energy = spectrum.calc_specific_energy(active_mass)
            specific_energies.append(specific_energy)
        return specific_energies

    def calc_specific_capacity(self, active_mass):
        return [spectrum.calc_specific_capacity(active_mass) for spectrum in self]

    def calc_areal_capacity(self, area):
        return [spectrum.calc_areal_capacity(area) for spectrum in self]

    def calc_volumetric_capacity(self, volume):
        return [spectrum.calc_volumetric_capacity(volume) for spectrum in self]

class XASSeries(Series):

    def __init__(self, iterable=None):
        Series.__init__(self, iterable)
        return

    def k_weight(self, weight=1, keep_params=True):
        weighted = XASSeries()
        for s in self:
            weighted.append(s.k_weight(weight=weight, keep_params=keep_params))
        return weighted

class LCMSSeries(Series):
    
    def __init__(self, iterable, time_axis):
        Series.__init__(self, iterable)
        self.retention_times = np.array(time_axis)
        return
    
    def _decompose_(self):
        spectra = [s.decompose() for s in self.spectra]
        retention_times = list(self.retention_times)
        return (spectra, retention_times)
    
    def _copy_empty_(self):
        copy = LCMSSeries(Series(), [])
        return copy
    
    def TIC(self):
        
        tic = Spectrum((self.retention_times, [s.sum() for s in self]))
        return tic
    
    def get_EIC(self, mz):
        intensities = []
        for spectrum in self:
            i = find_nearest_member(spectrum.x, mz)
            intensities.append(spectrum.y[i])
        
        eic = Spectrum((self.retention_times, intensities))
        return eic
        
    def get_spec(self, rt):
        i = find_nearest_member(self.retention_times, rt)
        return self[i]
    
    def show(self, *args, **kwds):
        '''
        Prints all spectra in series to new Matplotlib figure.
        '''
        if 'figsize' in kwds:
            plt.figure(figsize=kwds['figsize'])
        else:
            plt.figure()
        if 'cmap' in kwds:
            cmap=kwds['cmap']
        else:
            cmap='viridis'
        vectors = np.vstack([s.y for s in self])
        l, s = (len(self), self[0].x[-1]-self[0].x[0])
        im = plt.imshow(vectors, vmin=0, vmax=np.std(vectors)*5, #aspect=s/l,
                        extent=[self[0].x[0], self[0].x[-1], self.retention_times[-1], self.retention_times[0]], cmap=cmap)
        plt.colorbar()
        plt.xlabel('Mass-to-charge ratio (m/z)')
        plt.ylabel('Retention Time (sec)')
        if 'xlabel' in kwds: plt.xlabel(kwds['xlabel'])
        if 'ylabel' in kwds: plt.ylabel(kwds['ylabel'])
        if 'title' in kwds: plt.title(kwds['title'])
        if 'legend' in kwds: plt.legend(kwds['legend'])
        return im
    
    def interpolate(self, new_axis, keep_params=True):
        interpolated = self._copy_empty_()
        interpolated.retention_times = np.array(self.retention_times)
        for spectrum in self:
            interpolated.append(spectrum.interpolate(new_axis, keep_params))
        return interpolated
    
    def save(self, fname):
        
        hf = h5.File(fname, 'w')
        
        spectra = hf.create_dataset('spectra', shape=(len(self), 2, len(self[0])))
        time_ax = hf.create_dataset('retention_times', data=self.retention_times)
        
        for i, s in enumerate(self.spectra):
            spectra[i,0] = s.x
            spectra[i,1] = s.y
            
        hf.close()
        return
        
    @staticmethod
    def load(fname):
        
        hf = h5.File(fname, 'r')
        spectra = hf['spectra']
        spectra = Series([MassSpectrum((x, y)) for x, y in spectra])
        time_ax = hf['retention_times'][:]
        hf.close()
        spectra = LCMSSeries(spectra, time_ax)
        return spectra
        
        