# -*- coding: utf-8 -*-

class SST_NEXAFS:
    '''
    Data handling class for NEXAFS endstation at NSLS-II beamline SST-1.
    '''

    def __init__(self):
        return

    @staticmethod
    def load(fname, normalize=True):

        with open(fname) as f:
            lines = [line.rstrip() for line in f.readlines()]

        for i, line in enumerate(lines):
            if line[:5] == "-----":
                breakline = i
                break
        meta_header = lines[:breakline]
        body = lines[breakline + 1:]
        header = body[0].split(' ')
        while '' in header: header.remove('')
        body = body[1:]

        data_dict = {kwd:[] for kwd in header}
        for line in body:
            line = line.split(' ')
            while '' in line: line.remove('')
            for i, kwd in enumerate(header):
                val = float(line[i])
                data_dict[kwd].append(val)

        x = data_dict['Energy']
        norm = data_dict['M4C']
        tey = [data_dict['TEY'][i] / n for i, n in enumerate(norm)]
        pey = [data_dict['PEY'][i] / n for i, n in enumerate(norm)]
        fy = [data_dict['FY_CsI'][i] / n for i, n in enumerate(norm)]
        tey = Spectrum((x, tey))
        pey = Spectrum((x, pey))
        fy = Spectrum((x, fy))

        metadata_dict = {}
        metadata_dict['format'] = meta_header[0]
        metadata_dict['filename'] = os.path.asabs(fname)
        line = meta_header[1].split('"')[-1]
        date = line[12:].split(' ')[0]
        time = line[12:].split(' ')[2:4]
        hour, minute, second = time[0].split(':')
        if time[1] == 'PM': hour += 12
        metadata_dict['created'] = date + ' ' + f'{hour}:{minute}:{second}'
        metadata_dict['diffractionElement'] = meta_header[2].split('.')[0].split('=')[1][1:]
        metadata_dict['ringEnergy']= meta_header[2].split('=')[-1][1:]
        metadata_dict['E0'] = float(meta_header[3].split('=')[-1][1:])
        metadata_dict['numRegions'] = int(meta_header[4].split('=')[-1][1:])
        metadata_dict['srb'] = [int(i) for i in meta_header[5].split('=')[-1][1:].split(' ')]
        metadata_dict['srss'] = [float(i) for i in meta_header[6].split('=')[-1][1:].split(' ')]
        metadata_dict['srb'] = [int(i) for i in meta_header[5].split('=')[-1][1:].split(' ')]

        for k, v in metadata_dict.items():
            tey.parameters[k] = v
            pey.parameters[k] = v
            fy.parameters[k] = v

        return (tey, pey, fy)

class QAS:
    
    def __init__(self):
        return
    
    @staticmethod
    def load(fname):
        '''
        Load .dat data files from QAS beamline at NSLS-II. Loads both
        sample and reference spectra as a single Series.
        '''
        
        with open(fname) as f:
            lines = f.readlines()
        header = []
        for i, line in enumerate(lines):
            if line[0] == '#':
                header.append(line[2:])
            else:
                break
        body = lines[i:]
        for i, line in enumerate(body):
            line = line.rstrip().split(' ')
            while '' in line: line.remove('')
            body[i] = [float(v) for v in line]
        body = np.array(body)
        x = body[:,0]
        i0 = body[:,1]
        it = body[:,2]
        ir = body[:,3]
        iff = body[:,4]
        
        ref = np.log(it/ir)
        sample = np.log(i0/it)
        fluor = np.log(it/iff)
        ser = Series([XASSpectrum((x, y)) for y in [sample, ref, fluor]])
        return ser