# -*- coding: utf-8 -*-
"""
Created on Wed Mar 11 09:23:20 2020

@author: Steven King
"""

from .. import *

class Genesys:
    '''
    Data I/O handler for Genesys UV-Vis Spectrophotometer
    '''
    def __init__(self):
        return
    @staticmethod
    def load(fname):
        '''
        Load data files output by Genesys
        '''
        #Open file into new list, strip newline chars, split by space delimeter
        with open(fname, 'r') as f:
            lines = [line.rstrip().split(' ') for line in f.readlines()]
        #Strip wavelength and absorbance values from dumped list
        x = [float(line[0]) for line in lines]
        y = [float(line[1]) for line in lines]
        #Generate new GenesysSpectrum object
        spectrum = PhotoSpectrum((x, y))
        return spectrum
