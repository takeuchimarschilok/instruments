# -*- coding: utf-8 -*-
"""
Created on Mar 16 2020 9:19

@author: Steven King
"""

from ..core.series import ConfocalSeries, RamanSeries
from ..core.spectra import RamanSpectrum
import numpy as np

class Horiba:

    def __init__(self):
        return

    @staticmethod
    def load_series(fname):
        '''
        Load confocal Raman spectral series from Horiba exported .txt file,
        returns populated ConfocalSeries object

        Input:
            fname: str
                Absolute filename of data file to be imported

        Output:
            cube: ConfocalSeries
                Spatially organized confocal Raman dataset
        '''
        with open(fname, 'r') as f:
            lines = [line.rstrip() for line in f.readlines()]
        
        # Infer dimensional metadata from imported lines
        
        for i, line in enumerate(lines):
            lines[i] = line.replace(',','.').split('\t')
        ss = lines[0]
        lines = np.array(lines)
        while '' in ss: ss.remove('')
        ss = np.array(ss, dtype=np.float32)
        
        if lines[0,2] == '':
            flatmode = False
            zz = np.unique(np.array(lines[1:, 0], dtype=np.float32))
            xx = np.unique(np.array(lines[1:, 2], dtype=np.float32))
            yy = np.unique(np.array(lines[1:, 1], dtype=np.float32))
        else:
            flatmode = True
            zz = np.array([0], dtype=np.float32)
            xx = np.unique(np.array(lines[1:, 1], dtype=np.float32))
            yy = np.unique(np.array(lines[1:, 0], dtype=np.float32))
       
        ix = list(xx)
        iy = list(yy)
        iz = list(zz)
        lx = len(ix)
        ly = len(iy)
        lz = len(iz)
        ls = len(ss)
        xbounds = (min(ss), max(ss))
        #Initialize data structure as 4D array of 0s
        cube = np.zeros(shape=(lx, ly, lz, ls), dtype=np.float32)
        #Iterate through imported lines, populate data structure with spectra
        for spectrum in lines[1:]:
            if flatmode:
                y, x = np.array(spectrum[:2], dtype=np.float32)
                x = ix.index(x)
                y = iy.index(y)
                z = 0
                spectrum = np.array(spectrum[2:], dtype=np.float32)
            else:
                z, y, x = np.array(spectrum[:3], dtype=np.float32)
                x = ix.index(x)
                y = iy.index(y)
                z = iz.index(z)
                spectrum = np.array(spectrum[3:], dtype=np.float32)
            cube[x, y, z] = spectrum
        #Repackage as ConfocalSeries and return
        cube = ConfocalSeries(ss, cube)
        return cube

    @staticmethod
    def load_spectrum(fname):
        '''
        Load a single Raman spectrum from a from Horiba exported .txt file,
        returns RamanSpectrum object

        Input:
            fname: str
                Absolute filename of data file to be imported

        Output:
            spectrum: RamanSpectrum
                Raman spectrum data handler object
        '''
        with open(fname) as f:
            lines = f.readlines()
        raw_data = []
        for line in lines:
            line = line.rstrip().split('\t')
            w = float(line[0])
            i = float(line[1])
            raw_data.append([w, i])
        spectrum = np.array(raw_data).T
        spectrum = RamanSpectrum((spectrum[0], spectrum[1]))
        #Retain filename as metadata tag
        spectrum.parameters['filename'] = fname
        return spectrum
