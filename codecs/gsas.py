'''
gsas.py

Created on Fri Jun  10 11:49:10 2022

@author: Steven King

Codec for import/export of X-ray diffraction data to/from the
GSAS data processing platform.
'''

from .. import *

class GSAS:

    def __init__(self):
        return

    @staticmethod
    def load(fname, format='txt'):

        def read_txt(fname):
            with open(fname) as f:
                lines = [line.rstrip().split(' ') for line in f.readlines()]
            for line in lines:
                while '' in line:
                    line.remove('')
            header = lines[0]
            body = lines[1:]
            x = []
            y_obs = []
            weight = []
            y_calc = []
            y_bkg = []
            for line in body:
                line = [float(v) for v in line]
                x.append(line[0])
                y_obs.append(line[1])
                weight.append(line[2])
                y_calc.append(line[3])
                y_bkg.append(line[4])

            obs = XRDSpectrum((x, y_obs))
            calc = XRDSpectrum((x, y_calc))
            bkg = XRDSpectrum((x, y_bkg))
            sub = obs - bkg
            return Series([sub, obs, calc, bkg])

        format_dict = {'txt':read_txt}
        codec = format_dict[format]
        ser = codec(fname)
        return ser
