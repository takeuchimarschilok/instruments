# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 16:09:37 2020

@author: Steven King
Rietveld refinement is a scam invented by PIs to torture grad students.
"""

from .. import *
import numpy as np

class Rigaku:
    '''
    Data I/O class for Rigaku X-ray Powder Diffractometer
    '''
    def __init__(self):
        return

    @staticmethod
    def load(fname):

        def read_asc(fname):
            """
            Read .ASC formatted data files from Rigaku X-ray diffractometers.

            Parameters
            ----------
            fname : str
                Relative file name of target data file.

            Returns
            -------
            spec : Instruments.XRDSpectrum
                Imported X-ray diffraction spectrum data.
            """

            with open(fname, 'r') as f:
                lines = [line.rstrip().split(',') for line in f.readlines()]
            header = [line[0] for line in lines[:79]]
            body = lines[78:-3]
            for i, line in enumerate(body):
                body[i] = [int(v) for v in line]
            data_y = []
            for line in body:
                for item in line:
                    data_y.append(item)
            metadata = {}
            for line in header:
                if line == '' or '=' not in line: continue
                try: pre, post = line.split('=')
                except:
                    raise ValueError(line)
                if post == '': continue
                while '\t' in pre:
                    pre = pre[:-1]
                pre = pre[1:].lower()
                while post[0] == ' ':
                    post = post[1:]
                metadata[pre] = post
            data_x = np.arange(float(metadata['start']), float(metadata['stop']) + float(metadata['step']), float(metadata['step']))
            data_y = np.array(data_y)
            data = XRDSpectrum((data_x, data_y))
            data.parameters = metadata

            return data

        def read_ras(fname):
            """
            Read .RAS formatted data files from Rigaku X-ray diffractometers.

            Parameters
            ----------
            fname : str
                Relative file name of target data file.

            Returns
            -------
            spec : Instruments.XRDSpectrum
                Imported X-ray diffraction spectrum data.
            """
            
            with open(fname, encoding='Shift-JIS') as f:
                lines = f.readlines()
            lines = [line.rstrip().split(' ') for line in lines]
            for i, line in enumerate(lines):
                if line[0] == '*RAS_INT_START': data_start_line = i+1
                elif line[0] == '*RAS_INT_END': data_end_line = i
            data_block = lines[data_start_line:data_end_line]
            x = []
            y = []
            for line in data_block:
                x.append(float(line[0]))
                y.append(float(line[1]))
            spec = XRDSpectrum((x,y))
            return spec

        def read_xls(fname):
            print('EXCEL FORMAT SUPPORT IN-PROGRESS, PLEASE USE ASC FORMAT INSTEAD')
            print('File not loaded')
            # import pandas as pd
            # df = pandas.read_excel(fname)


        formats = {'asc':read_asc, 'ras':read_ras}
        file_format = fname.split('.')[-1]
        codec = formats[file_format]
        data = codec(fname)

        return data
