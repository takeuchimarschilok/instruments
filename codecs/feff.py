# -*- coding: utf-8 -*-
'''
feff.py

Created on Sat Nov  5 11:00:10 2022

@author: Steven King

Codec for import of X-ray absorption spectroscopy data from the
FEFF theoretical data generation package.
'''

from .. import *
import numpy as np

class Feff:
    
    def __init__(self):
        return
    
    @staticmethod
    def load(fname):
        
        def parse_chi_dat(fname='chi.dat'):
            
            with open(fname, 'r') as f:
                lines = [line.rstrip() for line in f.readlines()]
            k = []
            chi = []
            for line in lines:
                if line[0] == '#':
                    continue
                line = line.split(' ')
                while '' in line: line.remove('')
                line = [float(v) for v in line]
                k.append(line[0])
                chi.append(line[1])
            spec = ins.XAFSSpectrum((k,chi))
            return spec
        
        return parse_chi_dat(fname)

    