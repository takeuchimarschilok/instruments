# -*- coding: utf-8 -*-
"""
Created on Thu Jan 12 10:00:34 2023

@author: Tyler
"""

from ..core.core import Spectrum, Series

class PeakFit:
    
    def __init__(self):
        return
    
    @staticmethod
    def load(fname, astype=Spectrum, readpeaks=True):
        
        def parse_prn(lines):
            header = lines[:3]
            title = header[0][1:-1]
            x_label = header[1][1:-1]
            y_label = header[2][1:-1]
            body = lines[3:]
            line1 = str(body[0]).split(' ')
            while '' in line1: line1.remove('')
            ncols = len(line1)
            x = []
            y = []
            if readpeaks:
                peaks = [[] for i in range(ncols-2)]
            for line in body:
                line = line.split(' ')
                while '' in line: line.remove('')
                if '*' in line:
                    continue
                vals = [float(v) for v in line]
                x.append(vals[0])
                y.append(vals[1])
                if ncols > 2 and readpeaks:
                    for i, val in enumerate(vals[2:]):
                        peaks[i].append(val)
            spec = astype((x,y))
            if readpeaks:
                peaks = Series([astype((x, p)) for p in peaks])
                spec.parameters['peaks'] = peaks
            spec.parameters['title'] = title
            spec.parameters['x label'] = x_label
            spec.parameters['y label'] = y_label
            return spec
        
        def parse_dat(lines):
            header = lines[0]
            body = lines[1:]
            line1 = str(body[0]).split(' ')
            while '' in line1: line1.remove('')
            ncols = len(line1)
            x = []
            y = []
            if readpeaks:
                peaks = [[] for i in range(ncols-4)]
            for line in body:
                line = line.split(' ')
                while '' in line: line.remove('')
                vals = [float(v) for v in line]
                x.append(vals[0])
                y.append(vals[1])
                if ncols > 4 and readpeaks:
                    for i, val in enumerate(vals[4:]):
                        peaks[i].append(val)
            spec = astype((x,y))
            if readpeaks:
                peaks = Series([astype((x, p)) for p in peaks])
                spec.parameters['peaks'] = peaks
            return spec
        
        with open(fname, 'r') as f:
            lines = [line.rstrip() for line in f.readlines()]
        
        extension = fname.split('.')[-1]
        codecs = {'prn':parse_prn, 'dat':parse_dat}
        
        spec = codecs[extension](lines)
        return spec