# -*- coding: utf-8 -*-
"""
Created on Fri Dec 20 17:32:26 2019

@author: Steven King
"""

from ..core.spectra import PhotoSpectrum
from ..core.series import PhotoSeries

class Olis:
    '''
    Data I/O handler class for Olis UV-Vis Spectrophotometer
    '''
    def __init__(self):
        return

    @staticmethod
    def load(fname):
        '''
        Load spectrum from exported .asc data file
        '''
        with open(fname, 'r') as f:
            lines = [line.rstrip().split('\t') for line in f.readlines()]
        x = [float(line[0]) for line in lines]
        y = [float(line[1]) for line in lines]
        spectrum = PhotoSpectrum((x, y))
        return spectrum
