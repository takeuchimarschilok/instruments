'''
athena.py

Created on Fri Jul  9 11:25:10 2021

@author: Steven King

Codec for import/export of X-ray absorption spectroscopy data to/from the
Athena data processing platform.
'''

from .. import *
import numpy as np

class Athena:

    def __init__(self):
        return

    @staticmethod
    def load(fname):

        def parse_single(fname):
            with open(fname,'r') as f:
                lines = [line.rstrip() for line in f.readlines()]
            body = []
            for line in lines:
                if '#' in line:
                    continue
                else:
                    line = line.split(' ')
                    while '' in line: line.remove('')
                    body.append(line)
            x = []
            y = []
            for line in body:
                x.append(float(line[0]))
                y.append(float(line[1]))
            new_spec = XASSpectrum((x, y))
            return new_spec

        def parse_multi(fname):
            with open(fname) as f:
                lines = [line.rstrip().split(' ') for line in f.readlines()]

            #Separate header lines from body data lines
            header = []
            body = []
            for line in lines:
                while '' in line: line.remove('')
                if len(line) < 2: continue
                if '#' in line[0]:
                    line.remove('#')
                    header.append(line)
                else:
                    body.append(line)

            #Delete buffer line
            # del header[-2]
            #Parse data parameters to dict
            parameters = {}
            for line in header[1:-1]:
                param_name = line[0]
                #Drop hanging colon
                param_name = param_name[:-1]
                value = ''
                for item in line[1:]:
                    value += item + ' '
                value = value[:-1]
                parameters[param_name] = value

            #Parse data to new dict
            data = {}
            for line in header:
                if 'Column' in line[0] or 'energy' in line[0].lower():
                    data[line[0]] = []
            headers = list(data.keys())
            for line in body:
                for i, value in enumerate(line):
                    head = headers[i]
                    data[head].append(float(value))

            x_ax_key = headers[0]
            x_ax = data[x_ax_key]
            if len(headers) > 2:
                new_series = XASSeries()
            for key, value in data.items():
                if key == x_ax_key or len(value) == 0:
                    continue
                new_spec = XASSpectrum((x_ax, value))
                new_spec.parameters['x_axis'] = x_ax_key
                new_spec.parameters['name'] = key
                for p, v in parameters.items():
                    new_spec.parameters[p] = v
                if len(headers) > 2:
                    new_series.append(new_spec)

            if len(headers) > 2:
                return new_series
            else:
                return new_spec

        with open(fname) as f:
            lines = f.readlines()

        if 'multicolumn' in lines[1] or len(lines[3].split(' ')) > 3:
            data = parse_multi(fname)
        else:
            data = parse_single(fname)

        return data

    @staticmethod
    def save(fname, payload):
        '''
        Outputs a single spectrum to a plaintext file. Export of Series objects
        is not yet supported.
        '''

        if issubclass(type(payload), Spectrum):
            with open(fname, 'w') as f:
                for i in range(len(payload)):
                    line = '%f\t%f\n'%(payload.x[i],payload.y[i])
                    f.write(line)

        elif issubclass(type(payload), Series):
            head, tail = fname.split('.')
            for i, s in enumerate(payload):

                new_fname = head + '_%i.'%(i+1) + tail
                Athena.save(new_fname, s)

        else:
            raise TypeError('Data type not supported')

        return
