# -*- coding: utf-8 -*-
"""
Created on Sat Jan  4 10:46:10 2020

@author: Steven King
"""

import datetime

import numpy as np

from .. import *

class BioLogic:
    '''
    Data I/O handler for BioLogic Electrochemical Workstation
    '''
    def __init__(self):
        return

    @staticmethod
    def load(fname, read_metadata=True):
        '''
        Load data file into dictionary data and metadata objects

        Input:
            fname: str
                Absolute filename of data file to be imported.

        Output:
            data: dict
                Data arrays pulled from body of data file, with column header
                names as keywords
            metadata: dict
                Measurement metadata stripped from header of data file
        '''
        #Read in all lines from file
        with open(fname, encoding='iso-8859-1') as f:
            lines = [line.rstrip() for line in f.readlines()]
        #Infer number of lines in file header
        n_header_lines = int(lines[1][-2:])
        #Extract header lines from imported list of lines
        header = lines[:n_header_lines-2]
        #Grab header line from data columns, parse for data dict keywords
        column_header = lines[n_header_lines-1].split('\t')
        #Extract data lines, reorganize into dict with keywords parsed above
        data = np.array([line.split('\t') for line in lines[n_header_lines:]], dtype=float)
        data = {v:data[:,i] for i, v in enumerate(column_header)}

        #Parse remainder of file header to extract measurement metadata
        if read_metadata:

            metadata = {
                'channel':None,
                'timestamp':None,
                'device':None,
                'address':None,
                'EC-Lab version':None,
                'server version':None,
                'interpreter version':None
            }
            metadata['channel'] = int(header[5][17])
            # metadata['timestamp'] = datetime.datetime.strptime(header[12][25:], '%m/%d/%Y %H:%M:%S')
            metadata['device'] = header[18][10:]
            metadata['address'] = header[19][10:]
            metadata['EC-Lab version'] = header[20][19:25]
            metadata['server version'] = header[21][16:22]
            metadata['interpreter version'] = header[22][20:26]
        else:
            metadata = None

        # for line in header[32:]:
        #     head = line[:20]
        #     tail = line[20:]
        #     while head[-1] == ' ':
        #         head = head[:-1]
        #     metadata[head.lower()] = tail

        return data, metadata
