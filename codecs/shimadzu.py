# -*- coding: utf-8 -*-
"""
Created on Mon Dec 12 17:04:58 2022

@author: Tyler
"""

import datetime

import numpy as np

from .. import *

class LC:
    '''
    Data I/O handler for Raspberry Pi ADC for liquid chromatography.
    '''
    def __init__(self):
        return

    @staticmethod
    def load(fname):
        
        v2abs = lambda v: 1.24778749967269 * v - 0.00407269492419293
        
        with open(fname, 'r') as f:
            lines = [line.rstrip().split('\t') for line in f.readlines()]
        header = lines[0]
        body = lines[1:]
        n_channels = len(body[0]) - 1
        ser = Series()
        x = [float(line[0]) for line in body]
        for i in range(n_channels):
            channel = [float(line[i+1]) for line in body]
            channel = [v2abs(v) for v in channel]
            channel = Chromatogram((x, channel))
            ser.append(channel)

        return ser
