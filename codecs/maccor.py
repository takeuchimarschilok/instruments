# -*- coding: utf-8 -*-
"""
Created on Fri Jan  3 09:08:57 2020

@author: Steven King
"""

import xlrd
import numpy as np
import datetime

'''
Note that importing MACCOR XLS or XLSX files requires installation of xlrd
dependency. This can be installed via PyPi with: pip install xlrd
'''

from ..core.spectra import CVSpectrum
from ..core.series import CVSeries

class MACCOR:

    def __init__(self):
        return

    @staticmethod
    def load(fname):

        def load_csv(fname):

            with open(fname) as f:
                lines = [line.rstrip() for line in f.readlines()]

            timestamp_format = '%d/%m/%Y %I:%M:%S %p'
            export_timestamp = lines[0].split(',')[1]
            export_timestamp = datetime.datetime.strptime(export_timestamp, timestamp_format)
            test_timestamp = lines[1].split(',')[1]
            test_timestamp = datetime.datetime.strptime(test_timestamp, timestamp_format)
            headers = lines[2].split(',')[:-1]
            data = {h:[] for h in headers}
            column_types = {h:data_types[h] for h in headers}
            for line in lines[3:]:
                line = line.split(',')
                for i, h in enumerate(headers):
                    entry = line[i]
                    try:
                        entry = column_types[h](entry)
                    except:
                        entry = None
                    data[h].append(entry)
            return data

        def load_xls(fname):
            '''
            Load MACCOR data from XLS file into CVSeries object
            '''
            
            data_types['TestTime'] = float
            data_types['StepTime'] = float
            
            book = xlrd.open_workbook(fname)
            sheets = book.sheets()
            sheet = sheets[0]
            meta = sheet.row_values(0)
            col_headers = sheet.row_values(1)
            while '' in col_headers:
                col_headers.remove('')
            cycle_counter = col_headers.index('Cyc#')
            
            cycles = []
            for sheet in sheets:
                name = sheet.name
                if 'Data' in name:
                    cycles += [v for v in sheet.col_values(cycle_counter)[2:]]
                    
            cycles = np.array(cycles, dtype=np.uint8)
            data = {}
            for sheet in sheets:
                name = sheet.name
                if 'Data' in name:
                    for i, h in enumerate(col_headers):
                        if 'FLG' in h:
                            continue
                        try:
                            column = np.array(sheet.col_values(i)[2:], dtype=data_types[h])
                        except KeyError:
                            continue
                        if h in data:
                            data[h] = np.hstack([data[h],column])
                        else:
                            data[h] = column
                
            data_types['TestTime'] = datetime.datetime
            data_types['StepTime'] = datetime.datetime

            return data

        def split_cycles(data):
            keys = list(data.keys())
            for k in list(keys):
                if 'FLG' in k:
                    keys.remove(k)
                    
            cycle_base = data['Cyc#'][0]
            n_cycles = len(np.unique(data['Cyc#'])) - cycle_base + 1
            n_entries = len(data['Cyc#'])
            if n_cycles == 1:
                cycles = {i+1:{'charge':{h:[] for h in keys},'discharge':{h:[]  for h in keys}} for i in range(n_cycles)}
            else:
                cycles = {i+1:{'charge':{h:[] for h in keys},'discharge':{h:[]  for h in keys}} for i in range(n_cycles-1)}
                
            ii = 0
            while ii < n_entries:
                entry = {h:data[h][ii] for h in keys}
                if cycle_base == 0 and n_cycles == 1:
                    cycle_number = 1
                else:
                    cycle_number = entry['Cyc#'] - cycle_base + 1
                mode = str(entry['State'])
                if mode == 'D':
                    mode = 'discharge'
                elif mode == 'C':
                    mode = 'charge'
                else:
                    ii += 1
                    continue
                for h in keys:
                    try:
                        cycles[cycle_number][mode][h].append(entry[h])
                    except:
                        # print('PANIC')
                        pass
                ii += 1
            return cycles
        
        extensions = {'csv': load_csv,
                      'xls': load_xls}

        data_types = {'Cyc#': int,
                      'Step': int,
                      'TestTime': datetime.datetime,
                      'StepTime': datetime.datetime,
                      'mAmp-hr': float,
                      'Watt-hr': float,
                      'Amps': float,
                      'Volts': float,
                      'State': str,
                      'ES': int}
            
        extension = fname.split('.')[-1]
        codec = extensions[extension]
        data = codec(fname)
        
        split_data = split_cycles(data)

        return split_data
