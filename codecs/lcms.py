# -*- coding: utf-8 -*-
"""
Created on Mon Oct 30 14:14:04 2023

@author: Tyler
"""

from .. import *
import numpy as np
import h5py as h5

class LCMS:
    
    def __init__(self):
        return
    
    @staticmethod
    def load(fname):
        
        def read_csv():
            with open(fname, 'r') as f: lines = f.readlines()
            lines = [line.rstrip().split(',') for line in lines]
            header = lines[0]
            splitline = lines.index([''])
            body = lines[1:splitline]
            mass_axis = body[0][1:]
            mass_axis = np.array([float(v) for v in mass_axis])
            time_axis = np.array([float(s[0]) for s in body[1:]])
            intensity_axes = np.array([v[1:] for v in body[1:]], dtype=np.float64)
            
            series = Series()
            for i, s in enumerate(intensity_axes):
                spec = MassSpectrum((mass_axis, s))
                series.append(spec)
                series.parameters['rt'] = time_axis[i]
            series = LCMSSeries(series, time_axis)
            return series
        
        def read_h5():
            hf = h5.File(fname, 'r')
            spectra_dset = hf['spectra']
            spectra = Series([Spectrum(s[0], s[1]) for s in spectra_dset])
            time_ax = hf['retention_times'][:]
            series = LCMSSeries(spectra, time_ax)
            return series
        
        codecs = {'csv':read_csv,
                  'h5':read_h5}
        
        file_type = fname.split('.')[-1]
        try:
            codec = codecs[file_type]
        except KeyError:
            raise KeyError(f'No codec exists for the specified file type: {file_type}')

        series = codec()

        return series

    
            
        