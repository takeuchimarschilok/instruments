# -*- coding: utf-8 -*-
"""
setup.py

Created on Tue Nov 22 09:40:02 2022

@author: Tyler
"""

from distutils.core import setup

setup(
      name='Instruments',
      version='1.2',
      description='Data processing library for IESE',
      author='Steven King',
      author_email='king.steven@stonybrook.edu',
      url=r'https://bitbucket.org/takeuchimarschilok/instruments/src/master/',
      packages=[
          'core',
          'codecs'
          ],
      install_requires = [
        'numpy',
        'matplotlib',
        'scipy',
        'xlrd',
        'pymatgen',
        'scikit-learn',
        'h5py']
      )