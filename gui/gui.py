# -*- coding: utf-8 -*-
"""
Created on Thu Jan  9 11:17:26 2020

@author: Steven King
"""

from .core import *
import tkinter as tk
from .codecs.maccor import MACCOR
import matplotlib
matplotlib.use("TkAgg")
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

class tGUI:

    def __init__(self):
        self.root = tk.Tk()

        tk.Label(self.root, text='Instuments').grid(row=0,column=0)
#        tk.Button(self.root, text='Rigaku XRD', command=self.startXRDGUI).grid(row=1, column=0)
        tk.Button(self.root, text='MACCOR', command=self.startMACCORGUI).grid(row=2, column=0)
#        tk.Button(self.root, text='UV-Vis', command=self.startUVVisGUI).grid(row=3, column=0)
#        tk.Button(self.root, text='BioLogic', command=self.startEISGUI).grid(row=4, column=0)
#        tk.Button(self.root, text='Raman', command=self.startRamanGUI).grid(row=5, column=0)

        self.root.mainloop()

        return

    def startMACCORGUI(self):
        MACCORGUI(self)
        return

    def startXRDGUI(self):
        return

    def startUVVisGUI(self):
        return

    def startEISGUI(self):
        return

    def startRamanGUI(self):
        return

class MACCORGUI:

    def __init__(self, master):
        self.master = master
        self.root = tk.Tk()
        self.data = None

        tk.Label(self.root, text='Filename').grid(row=0, column=0)
        tk.Label(self.root, text='     ').grid(row=0, column=3) #Empty spacer Label
        fname = tk.StringVar()
        self.fname_entry = tk.Entry(self.root, textvariable=fname)
        self.fname_entry.grid(row=0, column=1)

        tk.Button(self.root, text='Import', command=self.load).grid(row=0, column=2)

        self.root.mainloop()

        return

    def load_workspace(self):
        tk.Label(self.root, text='Active mass (g)').grid(row=3, column=0)
        tk.Label(self.root, text='Surface area (m^2)').grid(row=4, column=0)
        tk.Label(self.root, text='Volume (m^3)').grid(row=5, column=0)
        active_mass = tk.DoubleVar()
        self.active_mass_entry = tk.Entry(self.root, textvariable=active_mass)
        self.active_mass_entry.grid(row=3, column=1)
        tk.Button(self.root, text='Calculate specific capacity', command=lambda: self.calc_specific_capacity(float(self.active_mass_entry.get()))).grid(row=3, column=2)
        tk.Button(self.root, text='Calculate specific energy', command=lambda: self.calc_specific_energy(float(self.active_mass_entry.get()))).grid(row=3, column=3)
        surface_area = tk.DoubleVar()
        self.surface_area_entry = tk.Entry(self.root, textvariable=surface_area)
        self.surface_area_entry.grid(row=4, column=1)
        tk.Button(self.root, text='Calculate areal capacity', command=lambda: self.calc_areal_capacity(float(self.surface_area_entry.get()))).grid(row=4, column=2)
        volume = tk.DoubleVar()
        self.volume_entry = tk.Entry(self.root, textvariable=volume)
        self.volume_entry.grid(row=5, column=1)
        tk.Button(self.root, text='Calculate volumetric capacity', command=lambda: self.calc_volumetric_capacity(float(self.volume_entry.get()))).grid(row=5, column=2)
        return

    def load(self):
        #Strip out any quotation marks from input filename
        fname = self.fname_entry.get()
        fname = [c for c in fname]
        while '"' in fname:
            fname.remove('"')
        fname = ''.join(fname)

        self.data = MACCOR.load(fname)
        f = Figure()
        a = f.add_subplot(111)
        dmax = -99999999999
        dmin = 99999999999
        for spectrum in self.data.spectra:
            a.plot(spectrum.charge['capacity'], spectrum.charge['voltage'], color='tab:blue')
            if max(spectrum.charge['voltage']) > dmax: dmax = max(spectrum.charge['voltage'])
            a.plot(spectrum.discharge['capacity'], spectrum.discharge['voltage'], color='tab:orange')
            if max(spectrum.discharge['voltage']) < dmin: dmin = max(spectrum.discharge['voltage'])
        a.set_ylim((dmin, dmax))
        a.set_xlabel('Capacity (A h)')
        a.set_ylabel('Voltage (V)')
        self._drawfigures_(f)
        self.load_workspace()
        return

    def calc_specific_capacity(self, active_mass):
        f1 = Figure()
        a1 = f1.add_subplot(111)
        f2 = Figure()
        a2 = f2.add_subplot(111)
        for spectrum in self.data.spectra:
            spectrum.calc_specific_capacity(active_mass)
        for i, spectrum in enumerate(self.data.spectra):
            a1.plot(spectrum.charge['specific_capacity'], spectrum.charge['voltage'], color='tab:blue')
            a1.plot(spectrum.discharge['specific_capacity'], spectrum.discharge['voltage'], color='tab:orange')
            a2.scatter(i+1, spectrum.charge['specific_capacity'][-1], color='tab:blue')
            a2.scatter(i+1, spectrum.discharge['specific_capacity'][-1], color='tab:orange')
        a1.set_xlabel('Specific Capacity (mAh/g)')
        a1.set_ylabel('Voltage (V)')
        a2.set_xlabel('Cycle #')
        a2.set_ylabel('Specific Capacity (mAh/g)')
        self._drawfigures_(f1, f2)
        return

    def calc_specific_energy(self, active_mass):
        f1 = Figure()
        a1 = f1.add_subplot(111)
        f2 = Figure()
        a2 = f2.add_subplot(111)
        for spectrum in self.data.spectra:
            spectrum.calc_specific_energy(active_mass)
        for i, spectrum in enumerate(self.data.spectra):
            a1.plot(spectrum.charge['specific_energy'], spectrum.charge['voltage'], color='tab:blue')
            a1.plot(spectrum.discharge['specific_energy'], spectrum.discharge['voltage'], color='tab:orange')
            a2.scatter(i+1, spectrum.charge['specific_energy'][-1], color='tab:blue')
            a2.scatter(i+1, spectrum.discharge['specific_energy'][-1], color='tab:orange')
        a1.set_xlabel('Specific Energy (Wh/g)')
        a1.set_ylabel('Voltage (V)')
        a2.set_xlabel('Cycle #')
        a2.set_ylabel('Specific Energy (Wh/g)')
        self._drawfigures_(f1, f2)
        return

    def calc_areal_capacity(self, area):
        f1 = Figure()
        a1 = f1.add_subplot(111)
        f2 = Figure()
        a2 = f2.add_subplot(111)
        for spectrum in self.data.spectra:
            spectrum.calc_areal_capacity(area)
        for i, spectrum in enumerate(self.data.spectra):
            a1.plot(spectrum.charge['areal_capacity'], spectrum.charge['voltage'], color='tab:blue')
            a1.plot(spectrum.discharge['areal_capacity'], spectrum.discharge['voltage'], color='tab:orange')
            a2.scatter(i+1, spectrum.charge['areal_capacity'][-1], color='tab:blue')
            a2.scatter(i+1, spectrum.discharge['areal_capacity'][-1], color='tab:orange')
        a1.set_xlabel('Areal Capacity (mAh/cm^2)')
        a1.set_ylabel('Voltage (V)')
        a2.set_xlabel('Cycle #')
        a2.set_ylabel('Areal Capacity (mAh/cm^2)')
        self._drawfigures_(f1, f2)
        return

    def calc_volumetric_capacity(self, volume):
        f1 = Figure()
        a1 = f1.add_subplot(111)
        f2 = Figure()
        a2 = f2.add_subplot(111)
        for spectrum in self.data.spectra:
            spectrum.calc_volumetric_capacity(volume)
        for i, spectrum in enumerate(self.data.spectra):
            a1.plot(spectrum.charge['volumetric_capacity'], spectrum.charge['voltage'], color='tab:blue')
            a1.plot(spectrum.discharge['volumetric_capacity'], spectrum.discharge['voltage'], color='tab:orange')
            a2.scatter(i+1, spectrum.charge['volumetric_capacity'][-1], color='tab:blue')
            a2.scatter(i+1, spectrum.discharge['volumetric_capacity'][-1], color='tab:orange')
        a1.set_xlabel('Volumetric Capacity (mAh/cm^3)')
        a1.set_ylabel('Voltage (V)')
        a2.set_xlabel('Cycle #')
        a2.set_ylabel('Volumetric Capacity (mAh/cm^3)')
        self._drawfigures_(f1, f2)
        return

    def _drawfigures_(self, f1, f2=None):
        canvas1 = FigureCanvasTkAgg(f1, self.root)
        canvas1.draw()
        canvas1.get_tk_widget().grid(row=0, column=300)
        if f2 is not None:
            canvas2 = FigureCanvasTkAgg(f2, self.root)
            canvas2.draw()
            canvas2.get_tk_widget().grid(row=0, column=301)
        return


if __name__ == "__main__":
    gui = tGUI()
